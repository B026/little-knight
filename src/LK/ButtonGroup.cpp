/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/ButtonGroup.hpp>

namespace lk
{
/////////////////////////////////////
ButtonGroup::ButtonGroup(float spacing, LinearLayout::Orientation orientation) : LinearLayout(spacing, orientation), toggledButton(nullptr),
																				 ignoreNextToggle(false)
{
}

/////////////////////////////////////
const Button * ButtonGroup::getToggledButton() const
{
	return toggledButton;
}

/////////////////////////////////////
Button * ButtonGroup::getToggledButton()
{
	return toggledButton;
}

/////////////////////////////////////
void ButtonGroup::onAddChild(Entity *child)
{
	LinearLayout::onAddChild(child);

	auto button = dynamic_cast <Button *>(child);

	if (! button)
		return;

	if (button->isToggled())
		toggledButton = button;

	button->onToggled.connect(std::bind(&ButtonGroup::onToggle, this, button));
}

/////////////////////////////////////
void ButtonGroup::onToggle(Button *clickedButton)
{
	if (ignoreNextToggle)
	{
		ignoreNextToggle = false;
		return;
	}

	// Toggle the toggled button
	if (toggledButton == clickedButton)
	{
		ignoreNextToggle = true;
		toggledButton->toggle();
	}
	else
	{
		auto prevToggleButton = toggledButton;
		toggledButton = clickedButton;
		ignoreNextToggle = true,
		prevToggleButton->toggle();
	}
}

} // namespace lk
