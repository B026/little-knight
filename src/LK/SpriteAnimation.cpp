/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/SpriteAnimation.hpp>

namespace lk
{
/////////////////////////////////////
SpriteAnimation::SpriteAnimation(const std::string &name, sf::Texture *texture, const Vector2D &frameSize, int numberOfFrames,
								 int row, int column) : name(name), texture(texture)
{
	const auto &textureSize = texture->getSize();
	int columns = textureSize.x / frameSize.x;
	int rows = textureSize.y / frameSize.y;

	Vector2D position(column * frameSize.x, row * frameSize.y);
	for (; row < rows; row++)
	{
		for (; column < columns; column++)
		{
			if (frames.size() == numberOfFrames)
				return;

			frames.emplace_back(position.x, position.y, frameSize.x, frameSize.y);
			position.x += frameSize.x;
		}

		position.x = 0;
		position.y += frameSize.y;
	}
}

/////////////////////////////////////
const std::string & SpriteAnimation::getName() const
{
	return name;
}

/////////////////////////////////////
sf::Texture * SpriteAnimation::getTexture() const
{
	return texture;
}

/////////////////////////////////////
const sf::IntRect & SpriteAnimation::operator[](int index) const
{
	if (index < 0 || index >= frames.size())
	{
		static sf::IntRect emptyRect;
		return emptyRect;
	}

	return frames [index];
}

/////////////////////////////////////
int SpriteAnimation::getFrameCount() const
{
	return frames.size();
}

} // namespace lk
