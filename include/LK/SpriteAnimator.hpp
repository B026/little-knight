/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#ifndef LITTLE_KNIGHT_SPRITE_ANIMATOR_HPP
#define LITTLE_KNIGHT_SPRITE_ANIMATOR_HPP

#include <LK/Sprite.hpp>
#include <LK/SpriteAnimation.hpp>
#include <LK/Action.hpp>

namespace lk
{

class SpriteAnimator : public Action
{
public:
	SpriteAnimator(const std::string &spriteAnimationName, float frameTime = 1.f / 60.f, int loopCount = Action::infiniteLoop, Sprite *target = nullptr);
	Action * clone() const override;
	void setTarget(Entity *target) override;
	void play() override;
	void stop() override;
	void onUpdate(float deltaTime) override;

private:
	int currentFrame;
	float frameTime;
	float currentFrameTime;
	SpriteAnimation *spriteAnimation;
	Sprite *targetSprite;
};

} // namespace lk

#endif // LITTLE_KNIGHT_SPRITE_ANIMATOR_HPP
