/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#ifndef LITTLE_KNIGHT_SIGNAL_DEF_INL
#define LITTLE_KNIGHT_SIGNAL_DEF_INL

#include <LK/SignalDef.hpp>

namespace lk
{
/////////////////////////////////////
template <typename... Args>
Signal <Args...>::Signal() : connectionsIdGenerator(0)
{
}

/////////////////////////////////////
template <typename... Args>
int Signal <Args...>::connect(const std::function <void (Args...)> &function, Entity *entity)
{
	connectionsIdGenerator++;
	functions.emplace_back(function, connectionsIdGenerator);

	if (entity)
		entity->onRelease.connect(std::bind(&Signal <Args...>::disconnect, this, connectionsIdGenerator));

	return connectionsIdGenerator;
}

/////////////////////////////////////
template <typename... Args>
void Signal <Args...>::disconnect(int connectionId)
{
	for (auto it = functions.begin(); it != functions.end(); ++it)
	{
		if (it->connectionId == connectionId)
		{
			functions.erase(it);
			return;
		}
	}
}

/////////////////////////////////////
template <typename... Args>
void Signal <Args...>::emit(Args... args)
{
	for (auto &function : functions)
		function.function(args...);
}

} // namespace lk

#endif // LITTLE_KNIGHT_SIGNAL_DEF_INL
