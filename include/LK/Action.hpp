/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#ifndef LITTLE_KNIGHT_ACTION_HPP
#define LITTLE_KNIGHT_ACTION_HPP

#include <LK/SignalDef.hpp>

namespace lk
{

class Entity;

///////////////////////////////////////
// Base class for animations.
//
// An action manipulates a target entity
// in a lapse of time which is the duration
// of the action. The number of times an
// action starts after reached the end is
// determined by loopCount.
//
// By default an action is State::Stopped, to play
// it use play(), pause() to pause it and
// stop() to stop it. To resume a State::Paused
// action use play(). Finally, restart() first
// stops the action and the plays it.
//
// An object of this class does nothing to
// its target entity, so it can be used as
// a delay animation when its part of a
// ActionList. Derived classes manipulate
// entity geometry like MoveTo, RotateBy, etc.
//
// If the offered actions are not enough for
// your needs, derive this class. The most
// importan function you must to reimplement
// are onUpdate() and clone().
///////////////////////////////////////
class Action
{
public:
	///////////////////////////////////////
	// Defines possible states of the action
	///////////////////////////////////////
	enum State
	{
		Stopped,	// The action is stopped, use Action::play() to start the action
		Running,	// The action is running, use Action::stop() to stop it or pause() to pause it
		Paused		// The action is paused, play() resumes the action and stop() stops the action
	};

	///////////////////////////////////////
	// Constructor, set duration, loopCount
	// and target.
	///////////////////////////////////////
	Action(float duration = 0.f, int loopCount = 1, Entity *target = nullptr);

	///////////////////////////////////////
	// Default destructor.
	///////////////////////////////////////
	virtual ~Action();

	///////////////////////////////////////
	// Clones the action.
	//
	// It's used when an action is added to
	// a ActionList. Basically returns a
	// copy of this action.
	///////////////////////////////////////
	virtual Action * clone() const;

	///////////////////////////////////////
	// Sets the target entity.
	///////////////////////////////////////
	virtual void setTarget(Entity *target);

	///////////////////////////////////////
	// Gets a const pointer to the target.
	///////////////////////////////////////
	const Entity * getTarget() const;

	///////////////////////////////////////
	// Gets a pointer to the target.
	///////////////////////////////////////
	Entity * getTarget();

	///////////////////////////////////////
	// Sets the duration of each loop
	// of the action.
	///////////////////////////////////////
	void setDuration(float duration);

	///////////////////////////////////////
	// Gets the duration of each loop of
	// the action.
	///////////////////////////////////////
	float getDuration() const;

	///////////////////////////////////////
	// Sets how many times an action runs.
	//
	// See loopCount
	///////////////////////////////////////
	void setLoopCount(int loopCount);

	///////////////////////////////////////
	// Gets the number of times an action runs.
	//
	// See loopCount
	///////////////////////////////////////
	int getLoopCount() const;

	///////////////////////////////////////
	// Tells if the action is running.
	///////////////////////////////////////
	bool isRunning() const;

	///////////////////////////////////////
	// Tells if the action is paused.
	///////////////////////////////////////
	bool isPaused() const;

	///////////////////////////////////////
	// Tells if the action is stopped
	///////////////////////////////////////
	bool isStopped() const;

	///////////////////////////////////////
	// Gets action's state
	///////////////////////////////////////
	State getState() const;

	///////////////////////////////////////
	// Starts or resumes the action.
	//
	// If the action is State::Stopped, it will start
	// only if the target entity is not nullptr.
	// If the action is State::Paused, it will resumes.
	///////////////////////////////////////
	virtual void play();

	///////////////////////////////////////
	// Pauses the action if it's State::Running.
	///////////////////////////////////////
	virtual void pause();

	///////////////////////////////////////
	// Stops the action.
	//
	// Resets currentTime and currentLoop and
	// emits onStop.
	///////////////////////////////////////
	virtual void stop();

	///////////////////////////////////////
	// Stops the action and then plays it.
	//
	// Is a shortcut for play() and stop()
	///////////////////////////////////////
	void restart();

	///////////////////////////////////////
	// If it's State::Running, updates action's members.
	//
	// This function is called at each frame,
	// updates currentTime and calls to onUpdate().
	// If currentTime overcomes duration, increases
	// currentLoop and resets currentTime. If
	// currentLoop overcomes loopCount the action
	// stops.
	///////////////////////////////////////
	virtual void update(float frameTime);

	///////////////////////////////////////
	// Actually does nothing.
	//
	// You must override this function in your
	// own actions an apply actions to the
	// target entity of your action.
	///////////////////////////////////////
	virtual void onUpdate(float frameTime);

	///////////////////////////////////////
	// Use this property to sets loopCount
	// as infinite.
	///////////////////////////////////////
	static int infiniteLoop;

	///////////////////////////////////////
	// Emitted once the action stops.
	//
	// It's emiited by stop()
	///////////////////////////////////////
	Signal <> onStop;

protected:
	///////////////////////////////////////
	// Sets the state of the action.
	//
	// Doesn't emit signals nor modify other
	// members, just state.
	///////////////////////////////////////
	void setState(State state);

	///////////////////////////////////////
	// Increases currentLoop by 1.
	///////////////////////////////////////
	void increaseCurrentLoop();

	///////////////////////////////////////
	// Gets currentLoop
	///////////////////////////////////////
	int getCurrentLoop() const;

	///////////////////////////////////////
	// The target entity of the action.
	//
	// It's affected by the action when this
	// is running. If it's nullptr, the action
	// will never start.
	//
	// **Related methods:**
	//
	// + setTarget()
	// + getTarget()
	///////////////////////////////////////
	Entity *target;

private:
	///////////////////////////////////////
	// Holds the duration of each loop
	// of the action is seconds.
	//
	// Sets it using setDuration() but only
	// if the action is State::Stopped, otherwise
	// it will not changed. Gets it using
	// getDuration()
	///////////////////////////////////////
	float duration;

	///////////////////////////////////////
	// Holds the current time of the loop.
	//
	// It's increased automatically by update()
	// and once it reachs duration, it's reset.
	//
	// When the action stops, it's set to 0
	///////////////////////////////////////
	float currentTime;

	///////////////////////////////////////
	// Indicates how many times the action runs.
	//
	// When currentLoop reaches this member,
	// the action stops.
	//
	// Set it in the constructor or using
	// setLoopCount(). Get it using getLoopCount().
	// If you want an action that runs until
	// you stop it manually, using stop(),
	// use infiniteLoop
	///////////////////////////////////////
	int loopCount;

	///////////////////////////////////////
	// Holds how many times the action has reached
	// the end.
	//
	// When it reaches loopCount, the action stops.
	// Get it using getCurrentLoop().
	//
	// It's increased automatically when currentTime
	// reaches duration or by calling increaseCurrentLoop().
	// It resets when the action stops.
	///////////////////////////////////////
	int currentLoop;

	///////////////////////////////////////
	// Holds the state of the action.
	//
	// A State::Stopped action just can start using play().
	// A State::Running action can stops and pauses.
	// A State::Paused action can stops and resumes.
	///////////////////////////////////////
	State state;
};

} // namespace lk

#endif // LITTLE_KNIGHT_ACTION_HPP
