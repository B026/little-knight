/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/SpriteAnimator.hpp>
#include <LK/AssetsManager.hpp>

namespace lk
{
/////////////////////////////////////
SpriteAnimator::SpriteAnimator(const std::string &spriteAnimationName, float frameTime, int loopCount, Sprite *target) :
							   Action(0, loopCount, target), spriteAnimation(AssetsManager::getSpriteAnimation(spriteAnimationName)),
							   frameTime(frameTime), targetSprite(target), currentFrame(-1), currentFrameTime(0)
{
	if (spriteAnimation)
		setDuration(spriteAnimation->getFrameCount() * frameTime);
}

/////////////////////////////////////
Action * SpriteAnimator::clone() const
{
	return new SpriteAnimator(*this);
}

/////////////////////////////////////
void SpriteAnimator::setTarget(Entity *target)
{
	Action::setTarget(target);
	targetSprite = dynamic_cast <Sprite *>(target);
}

/////////////////////////////////////
void SpriteAnimator::play()
{
	if (! targetSprite || ! spriteAnimation || ! spriteAnimation->getFrameCount())
		return;

	currentFrame = 0;
	targetSprite->setTexture(spriteAnimation->getTexture());
	targetSprite->setTextureRect((*spriteAnimation) [currentFrame]);

	Action::play();
}

/////////////////////////////////////
void SpriteAnimator::stop()
{
	Action::stop();
	currentFrame = -1;
	currentFrameTime = 0.f;
}

/////////////////////////////////////
void SpriteAnimator::onUpdate(float deltaTime)
{
	currentFrameTime += deltaTime;

	if (currentFrameTime < frameTime)
		return;

	currentFrameTime = 0;
	currentFrame++;

	if (currentFrame < spriteAnimation->getFrameCount())
	{
		targetSprite->setTextureRect((*spriteAnimation) [currentFrame]);
	}
	else
	{
		currentFrame = 0;
		targetSprite->setTextureRect((*spriteAnimation) [currentFrame]);
	}
}

} // namespace lk
