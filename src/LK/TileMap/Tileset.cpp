/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/TileMap/Tileset.hpp>
#include <LK/AssetsManager.hpp>

namespace lk
{
/////////////////////////////////////
Tileset::Tileset(const std::string &name, int tileIdGenerator) : texture(nullptr), name(name), tileIdGenerator(tileIdGenerator)
{
}

/////////////////////////////////////
Tileset::Tileset(sf::Texture *texture, const Point &tileSize, const std::string &name, int tileIdGenerator) :
				 texture(nullptr), name(name), tileIdGenerator(tileIdGenerator)
{
	load(texture, tileSize);
}

/////////////////////////////////////
Tileset::Tileset(const std::string &textureFilename, const Point &tileSize, const std::string &name, int tileIdGenerator) :
				 texture(nullptr), name(name), tileIdGenerator(tileIdGenerator)
{
	load(AssetsManager::getTexture(textureFilename), tileSize);
}

/////////////////////////////////////
void Tileset::load(const std::string &textureFilename, const Point &tileSize)
{
	load(AssetsManager::getTexture(textureFilename), tileSize);
}

/////////////////////////////////////
void Tileset::load(sf::Texture *texture, const Point &tileSize)
{
	this->tileSize = tileSize;
	tiles.clear();
	this->texture = texture;
	tileIdGenerator = 0;

	if (! texture)
		return;

	const auto &textureSize = texture->getSize();

	int textureColumnCount = textureSize.x / tileSize.x;
	int textureRowCount = textureSize.y / tileSize.y;

	for (int row = 0; row < textureRowCount; row++)
		for (int column = 0; column < textureColumnCount; column++)
			tiles.emplace_back(Point(column * tileSize.x, row * tileSize.y), tileIdGenerator++);
}

/////////////////////////////////////
const std::string & Tileset::getName() const
{
	return name;
}

/////////////////////////////////////
void Tileset::setName(const std::string &name)
{
	this->name = name;
}

/////////////////////////////////////
const Tile * Tileset::getTile(int tileId) const
{
	for (auto &tile : tiles)
		if (tile.id == tileId)
			return &tile;

	return nullptr;
}

/////////////////////////////////////
const Point & Tileset::getTileSize() const
{
	return tileSize;
}

/////////////////////////////////////
const sf::Texture * Tileset::getTexture() const
{
	return texture;
}

} // namespace lk
