/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#ifndef LITTLE_KNIGHT_ACTION_LIST_HPP
#define LITTLE_KNIGHT_ACTION_LIST_HPP

#include <LK/Action.hpp>
#include <vector>

namespace lk
{

///////////////////////////////////////
// A list of actions which are applied
// consecutively to the same target entity.
//
// To push actions to the list use pushAction(),
// clear() lets you remove all the actions
// in the list.
///////////////////////////////////////
class ActionList : public Action
{
public:
	///////////////////////////////////////
	// Default constructor
	///////////////////////////////////////
	ActionList();

	///////////////////////////////////////
	// Default destructor. Calls to clear()
	///////////////////////////////////////
	~ActionList();

	///////////////////////////////////////
	// Appends a given action to the list.
	//
	// If the action doesn't have a target
	// entity, sets the target entity of
	// the list to it (which is the normal
	// behavior).
	//
	// The given action must to have implemented
	// Action::clone()
	///////////////////////////////////////
	void pushAction(const Action &action);

	///////////////////////////////////////
	// Removes all the actions in the list.
	//
	// Before to remove the actions, the
	// list stops.
	///////////////////////////////////////
	void clear();

	///////////////////////////////////////
	// Plays the first action in the list.
	//
	// If there are not actions in the list
	// it will never start.
	///////////////////////////////////////
	void play() override;

	///////////////////////////////////////
	// Pauses the current action.
	///////////////////////////////////////
	void pause() override;

	///////////////////////////////////////
	// Stops the current action.
	//
	// Sets currentAction to nullptr and
	// currentActionIndex to -1.
	//
	// Calls to Action::stop()
	///////////////////////////////////////
	void stop() override;

	///////////////////////////////////////
	// Updates the current action.
	//
	// Once the current action stops, moves to
	// the next action in the list.
	///////////////////////////////////////
	void update(float deltaTime) override;

private:
	///////////////////////////////////////
	// Holds the currentAction index in the
	// actions list.
	//
	// It's -1 by default and it's set to 0
	// when the list starts. Increases when
	// currentAction stops
	///////////////////////////////////////
	int currentActionIndex;

	///////////////////////////////////////
	// Holds the current action which is being
	// running.
	//
	// It's nullptr by default and when the action
	// is stopped, but when the actions starts
	// it's set to the first action in the list.
	//
	// It's changed each time the current action
	// stops.
	///////////////////////////////////////
	Action *currentAction;

	///////////////////////////////////////
	// The list of actions which are executed
	// one by one consecutively.
	//
	// Use pushAction() to appends an action
	// to the list and clear() to remove everyone.
	///////////////////////////////////////
	std::vector <Action *> actions;
};

} // namespace lk

#endif // LITTLE_KNIGHT_ACTION_LIST_HPP
