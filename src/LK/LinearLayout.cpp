/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/LinearLayout.hpp>

namespace lk
{
///////////////////////////////////////
LinearLayout::LinearLayout(float spacing, Orientation orientation) : spacing(spacing), orientation(orientation), childrenNeedUpdate(true)
{
}

///////////////////////////////////////
LinearLayout::Orientation LinearLayout::getOrientation() const
{
	return orientation;
}

///////////////////////////////////////
void LinearLayout::setOrientation(Orientation orientation)
{
	if (this->orientation == orientation)
		return;

	this->orientation = orientation;
	childrenNeedUpdate = true;
}

///////////////////////////////////////
float LinearLayout::getSpacing() const
{
	return spacing;
}

///////////////////////////////////////
void LinearLayout::setSpacing(float spacing)
{
	if (this->spacing == spacing)
		return;

	this->spacing = spacing;
	childrenNeedUpdate = true;
}

///////////////////////////////////////
void LinearLayout::forceUpdate()
{
	updateChildren();
}

///////////////////////////////////////
void LinearLayout::onAddChild(Entity *child)
{
	childrenNeedUpdate = true;
}

///////////////////////////////////////
void LinearLayout::onRemoveChild(Entity *child)
{
	childrenNeedUpdate = true;
}

///////////////////////////////////////
void LinearLayout::updateChildren()
{
	Vector2D size;
	const auto &children = getChildren();

	// Calculate layout size
	for (auto child : children)
	{
		const auto &childBoundingBox = child->getFullBoundingBox();
		if (orientation == Horizontal)
		{
			size.x += childBoundingBox.width;
			size.y = (! size.y || size.y < childBoundingBox.height) ? childBoundingBox.height : size.y;
		}
		else
		{
			size.y += childBoundingBox.height;
			size.x = (! size.x || size.x < childBoundingBox.width) ? childBoundingBox.width : size.x;
		}
	}

	// Add padding to its size
	if (orientation == Horizontal)
		size.x += spacing * (children.size() - 1);
	else
		size.y += spacing * (children.size() - 1);

	// Reposition children
	float left = - size.x * 0.5f;
	float top = - size.y * 0.5f;

	for (auto child : children)
	{
		const auto &childBoundingBox = child->getFullBoundingBox();
		child->setOriginCenter();

		if (orientation == Horizontal)
		{
			child->setPosition(left + childBoundingBox.width * 0.5f, 0);
			left += childBoundingBox.width + spacing;
		}
		else
		{
			child->setPosition(0, top + childBoundingBox.height * 0.5);
			top += childBoundingBox.height + spacing;
		}
	}

	childrenNeedUpdate = false;
}

///////////////////////////////////////
void LinearLayout::onUpdate(float frameTime)
{
	if (childrenNeedUpdate)
		updateChildren();
}

} // namespace lk
