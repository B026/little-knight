/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#ifndef LITTLE_KNIGHT_SPRITE_ANIMATION_HPP
#define LITTLE_KNIGHT_SPRITE_ANIMATION_HPP

#include <LK/Vector2D.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <string>
#include <vector>

namespace lk
{

class SpriteAnimation
{
public:
	SpriteAnimation(const std::string &name, sf::Texture *texture, const Vector2D &frameSize, int numberOfFrames = -1,
					int row = 0, int column = 0);

	const std::string & getName() const;
	sf::Texture * getTexture() const;
	int getFrameCount() const;
	const sf::IntRect & operator[](int index) const;

private:
	sf::Texture *texture;
	std::string name;
	std::vector <sf::IntRect> frames;
};

} // namespace lk

#endif //  LITTLE_KNIGHT_SPRITE_ANIMATION_HPP
