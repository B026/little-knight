/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#ifndef LITTLE_KNIGHT_TILE_MAP_TILE_LAYER_HPP
#define LITTLE_KNIGHT_TILE_MAP_TILE_LAYER_HPP

#include <LK/TileMap/Tile.hpp>
#include <LK/TileMap/Tileset.hpp>
#include <LK/Entity.hpp>
#include <SFML/Graphics/VertexArray.hpp>

namespace lk
{

class TileMap;

class TileLayer : public Entity
{
public:
	TileLayer(const std::string &name, const TileMap *tileMap, const Tileset *tileset);
	void setSource(int source []);
	void reset(int value = -1);
	int get(const Point &tilePosition) const;
	int get(int x, int y) const;
	Point getPositionOf(int value) const;
	void set(const Point &tilePosition, int value);
	void set(int x, int y, int value);
	void set(int index, int value);
	void swap(const Point &tilePositionA, const Point &tilePositionB);

private:
	void updateVertices() const;
	void onDraw(sf::RenderTarget &target, sf::RenderStates states) const override;

	const TileMap *tileMap;
	const Tileset *tileset;
	mutable bool verticesNeedUpdate;
	std::vector <int> source;
	mutable sf::VertexArray vertices;
};

} // namespace lk

#endif // LITTLE_KNIGHT_TILE_MAP_TILE_LAYER_HPP
