/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/Widget.hpp>
#include <LK/Director.hpp>

namespace lk
{
/////////////////////////////////////
Widget::Widget(bool focusable) : focusable(focusable), state(Widget::Normal)
{
	setScene(Director::getScene());
}

/////////////////////////////////////
void Widget::setScene(Scene *scene)
{
	if (! scene)
		return;

	scene->connectToEvent(sf::Event::MouseMoved, std::bind(&Widget::mouseMovedEvent, this, std::placeholders::_1), this);
	scene->connectToEvent(sf::Event::MouseButtonPressed, std::bind(&Widget::mouseButtonPressedEvent, this,
						  std::placeholders::_1), this);
	scene->connectToEvent(sf::Event::MouseButtonReleased, std::bind(&Widget::mouseButtonReleasedEvent, this,
						  std::placeholders::_1), this);
}

/////////////////////////////////////
Widget::State Widget::getState() const
{
	return state;
}

/////////////////////////////////////
void Widget::mouseMovedEvent(const sf::Event &event)
{
	switch (state)
	{
		case Normal:
			if (getFullBoundingBox().contains(event.mouseMove.x, event.mouseMove.y))
			{
				setState(Hover);
				onMouseOver.emit();
			}
		break;

		case Hover:
		case Pressed:
			if (! getFullBoundingBox().contains(event.mouseMove.x, event.mouseMove.y))
			{
                setState(Normal);
				onMouseLeave.emit();
			}
		break;
	}
}

/////////////////////////////////////
void Widget::mouseButtonPressedEvent(const sf::Event &event)
{
	switch (state)
	{
		case Hover:
			setState(Pressed);

			if (event.mouseButton.button == sf::Mouse::Left)
				onMouseLeftPressed.emit();
			else
				onMouseRightPressed.emit();
		break;

		case Focus:
			if (getFullBoundingBox().contains(event.mouseButton.x, event.mouseButton.y))
			{
				if (event.mouseButton.button == sf::Mouse::Left)
					onMouseLeftPressed.emit();
				else
					onMouseRightPressed.emit();
			}
	}
}

/////////////////////////////////////
void Widget::mouseButtonReleasedEvent(const sf::Event &event)
{
	switch (state)
	{
		case Pressed:
			if (event.mouseButton.button == sf::Mouse::Left)
				onClick.emit();
			else
				onRightClick.emit();

			if (focusable)
			{
                setState(Focus);
				onFocus.emit();
			}
			else
                setState(Hover);
		break;

		case Focus:
    		if (getFullBoundingBox().contains(event.mouseButton.x, event.mouseButton.y))
    		{
    			if (event.mouseButton.button == sf::Mouse::Left)
    				onClick.emit();
    			else
    				onRightClick.emit();
            }
			else
			{
				setState(Normal);
				onBlur.emit();
			}
	}
}

/////////////////////////////////////
void Widget::setState(State state)
{
    if (this->state == state)
        return;

    this->state = state;
    onStateChanged.emit(state);
}

} // namespace lk
