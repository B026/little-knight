/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/JSON.hpp>
#include <stdio.h>

namespace lk
{
///////////////////////////////////////
std::string JSON::emptyString;

///////////////////////////////////////
JSON::Value::Value(const std::string &name, const std::string &value) : name(name), value(value)
{
}

///////////////////////////////////////
JSON::JSON(const std::string &jsonString)
{
	fromString(jsonString);
}

///////////////////////////////////////
void JSON::fromString(const std::string &jsonString)
{
	if (jsonString.empty())
		return;

	values.clear();

	// Interpret the string
	auto it = jsonString.begin();
	auto end = jsonString.end();
	std::string name;
	std::string value;
	char openingQuotationMark;

	while (it != end)
	{
		name.clear();
		value.clear();

		// Read until opening quotation mark
		while (*it != '"' && *it != '\'')
		{
			++it;
			if (it == end) return;
		}

		// Eat opening quotation mark
		openingQuotationMark = *it;
		++it;
		if (it == end) return;

		// Read name until ending quotation mark
		while (*it != openingQuotationMark)
		{
			name.push_back(*it);
			++it;
			if (it == end) return;
		}

		// Eat ending quotation mark
		++it;
		if (it == end) return;

		// Read until :
		while (*it != ':')
		{
			++it;
			if (it == end) return;
		}

		// Eat :
		++it;
		if (it == end) return;

		// Read white spaces, \t and \n
		while (*it == ' ' || *it == '\t' || *it == '\n')
		{
			++it;
			if (it == end) return;
		}

		// Read value until , or end
		while (*it != ',' && it != end)
		{
			value.push_back(*it);
			++it;
		}

		values.emplace_back(name, value);
	}
}

///////////////////////////////////////
void JSON::set(const std::string &name, int value)
{
	set(name, std::to_string(value));
}

///////////////////////////////////////
void JSON::set(const std::string &name, bool value)
{
	set(name, (value) ? "1" : "0");
}

///////////////////////////////////////
void JSON::set(const std::string &name, float value)
{
	set(name, std::to_string(value));
}

///////////////////////////////////////
void JSON::set(const std::string &name, const std::string &value)
{
	// Looking for it
	for (auto &valueIt : values)
	{
		if (valueIt.name == name)
		{
			valueIt.value = value;
			return;
		}
	}

	values.emplace_back(name, value);
}

///////////////////////////////////////
int JSON::getInt(const std::string &name) const
{
	return std::stoi(getValue(name));
}

///////////////////////////////////////
bool JSON::getBool(const std::string &name) const
{
	const auto &value = getValue(name);
	return (value == "1" || value == "true") ? true : false;
}

///////////////////////////////////////
float JSON::getFloat(const std::string &name) const
{
	return std::stof(getValue("name"));
}

///////////////////////////////////////
const std::string & JSON::getValue(const std::string &name) const
{
	for (auto &value : values)
		if (value.name == name)
			return value.value;

	return emptyString;
}

} // namespace lk
