/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/Button.hpp>

namespace lk
{
/////////////////////////////////////
/*Button::Button(bool toggle, Type type) : Widget(false), componentsNeedUpdate(true), toggleButton(toggle), toggled(false), type(type), spaceBetween(5)
{
	sprite = new Sprite;
	sprite->setName("spriteComponent");
	addChild(sprite);

	text = new Text;
	text->setName("textComponent");
	addChild(text);

	onClick.connect(std::bind(&Button::toggle, this));
}*/

/////////////////////////////////////
Button::Button(const std::string &string, const std::string &font, int fontSize, bool toggle, Type type) :
			   Widget(false), componentsNeedUpdate(true), toggleButton(toggle), toggled(false), type(type), spaceBetween(5)

{
	sprite = new Sprite;
	sprite->setName("spriteComponent");
	addChild(sprite);

	text = new Text(string, font, fontSize);
	text->setName("textComponent");
	addChild(text);

	onClick.connect(std::bind(&Button::toggle, this));
}

/////////////////////////////////////
Button::Button(const std::string &string, sf::Font *font, int fontSize, bool toggle, Type type) :
			   Widget(false), componentsNeedUpdate(true), toggleButton(toggle), toggled(false), type(type), spaceBetween(5)
{
	sprite = new Sprite;
	sprite->setName("spriteComponent");
	addChild(sprite);

	text = new Text(string, font, fontSize);
	text->setName("textComponent");
	addChild(text);

	onClick.connect(std::bind(&Button::toggle, this));
}

/////////////////////////////////////
Button::Button(const std::string &texture, bool toggle, Type type) :
			   Widget(false), componentsNeedUpdate(true), toggleButton(toggle), toggled(false), type(type), spaceBetween(5)
{
	sprite = texture.length() ? new Sprite(texture) : new Sprite;
	sprite->setName("spriteComponent");
	addChild(sprite);

	text = new Text;
	text->setName("textComponent");
	addChild(text);

	onClick.connect(std::bind(&Button::toggle, this));
}

/////////////////////////////////////
Button::Button(const std::string &texture, const std::string &string, const std::string &font, int fontSize, bool toggle, Type type) :
			   Widget(false), componentsNeedUpdate(true), toggleButton(toggle), toggled(false), type(type), spaceBetween(5)
{
	sprite = new Sprite(texture);
	sprite->setName("spriteComponent");
	addChild(sprite);

	text = new Text(string, font, fontSize);
	text->setName("textComponent");
	addChild(text);

	onClick.connect(std::bind(&Button::toggle, this));
}

/////////////////////////////////////
Button::Button(const std::string &texture, const std::string &string, sf::Font *font, int fontSize, bool toggle, Type type) :
			   Widget(false), componentsNeedUpdate(true), toggleButton(toggle), toggled(false), type(type), spaceBetween(5)
{
	sprite = new Sprite(texture);
	sprite->setName("spriteComponent");
	addChild(sprite);

	text = new Text(string, font, fontSize);
	text->setName("textComponent");
	addChild(text);

	onClick.connect(std::bind(&Button::toggle, this));
}

/////////////////////////////////////
void Button::setType(Type type)
{
	if (this->type == type)
		return;

	this->type = type;
	componentsNeedUpdate = true;
}

/////////////////////////////////////
Button::Type Button::getType() const
{
	return type;
}

/////////////////////////////////////
void Button::setSpaceBetween(int spaceBetween)
{
	if (this->spaceBetween == spaceBetween)
		return;

	this->spaceBetween = spaceBetween;
	componentsNeedUpdate = true;
}

/////////////////////////////////////
int Button::getSpaceBetween() const
{
	return spaceBetween;
}


/////////////////////////////////////
const sf::Texture * Button::getTexture() const
{
	return sprite->getTexture();
}

/////////////////////////////////////
sf::Texture * Button::getTexture()
{
	return sprite->getTexture();
}

/////////////////////////////////////
void Button::setTexture(const std::string &texturePath, bool resetRect)
{
	setTexture(AssetsManager::getTexture(texturePath), resetRect);
}

/////////////////////////////////////
void Button::setTexture(sf::Texture *texture, bool resetRect)
{
	sprite->setTexture(texture, resetRect);
	componentsNeedUpdate = true;
}

/////////////////////////////////////
void Button::setTextureRect(const sf::IntRect &rect)
{
	sprite->setTextureRect(rect);
	componentsNeedUpdate = true;
}

/////////////////////////////////////
void Button::setText(const sf::String &string, const std::string &fontPath, UInt fontSize)
{
	setText(string, AssetsManager::getFont(fontPath), fontSize);
}

/////////////////////////////////////
void Button::setText(const sf::String &string, sf::Font *font, UInt fontSize)
{
	text->setString(string);
	text->setFont(font);
	text->setFontSize(fontSize);
	componentsNeedUpdate = true;
}

/////////////////////////////////////
const sf::String & Button::getString() const
{
	return text->getString();
}

/////////////////////////////////////
void Button::setString(const sf::String &string)
{
	text->setString(string);
	componentsNeedUpdate = true;
}

/////////////////////////////////////
const sf::Font * Button::getFont() const
{
	return text->getFont();
}

/////////////////////////////////////
sf::Font * Button::getFont()
{
	return text->getFont();
}

/////////////////////////////////////
void Button::setFont(const std::string &fontPath)
{
	setFont(AssetsManager::getFont(fontPath));
}

/////////////////////////////////////
void Button::setFont(sf::Font *font)
{
	text->setFont(font);
	componentsNeedUpdate = true;
}

/////////////////////////////////////
bool Button::isBold() const
{
	return text->isBold();
}

/////////////////////////////////////
void Button::setBold(bool bold)
{
	text->setBold(bold);
	componentsNeedUpdate = true;
}

/////////////////////////////////////
UInt Button::getFontSize() const
{
	return text->getFontSize();
}

/////////////////////////////////////
void Button::setFontSize(UInt fontSize)
{
	text->setFontSize(fontSize);
	componentsNeedUpdate = true;
}

/////////////////////////////////////
float Button::getOutlineThickness() const
{
	return text->getOutlineThickness();
}

/////////////////////////////////////
void Button::setOutlineThickness(float thickness)
{
	text->setOutlineThickness(thickness);
	componentsNeedUpdate = true;
}

/////////////////////////////////////
const Color & Button::getColor() const
{
	return text->getColor();
}

/////////////////////////////////////
void Button::setColor(const Color &color)
{
	text->setColor(color);
	sprite->setColor(color);
}

/////////////////////////////////////
void Button::setSpriteColor(const Color &color)
{
	sprite->setColor(color);
}

/////////////////////////////////////
void Button::setTextColor(const Color &color)
{
	text->setColor(color);
}

/////////////////////////////////////
const Color & Button::getOutlineColor() const
{
	return text->getOutlineColor();
}

/////////////////////////////////////
void Button::setOutlineColor(const Color &color)
{
	text->setOutlineColor(color);
}

/////////////////////////////////////
bool Button::setProperty(const XMLAttribute &property)
{
	return (Entity::setProperty(property) || sprite->setProperty(property) || text->setProperty(property));
}

/////////////////////////////////////
bool Button::isToggle() const
{
	return toggleButton;
}

/////////////////////////////////////
void Button::setToggle(bool toggle)
{
	toggleButton = toggle;
}

/////////////////////////////////////
bool Button::isToggled() const
{
	return toggled;
}

/////////////////////////////////////
void Button::toggle()
{
	if (toggleButton)
	{
		toggled = ! toggled;
		onToggled.emit();
	}
}

/////////////////////////////////////
void Button::onDraw(sf::RenderTarget &target, sf::RenderStates states) const
{
	if (! componentsNeedUpdate)
		return;

	sprite->setOriginCenter();
	text->setOriginCenter();
	componentsNeedUpdate = false;

	if (type == Checkbox)
	{
		float spriteWidth = sprite->getSize().x;
		float textWidth = text->getSize().x;
		float halftWidth = (spriteWidth + spaceBetween + textWidth) * 0.5f;
		float left = - halftWidth;

		text->setPosition(left + textWidth * 0.5f, 0);
		left += textWidth + spaceBetween;
		sprite->setPosition(left + spriteWidth * 0.5f, 0);
	}
}

} // namespace lk
