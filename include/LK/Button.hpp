/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#ifndef LITTLE_KNIGHT_BUTTON_HPP
#define LITTLE_KNIGHT_BUTTON_HPP

#include <LK/Sprite.hpp>
#include <LK/Text.hpp>
#include <LK/Widget.hpp>

namespace lk
{

///////////////////////////////////////
// Button widget.
//
// A button consists of 2 elements: a sprite
// and a text. You can use both or just one
// of them to describe graphically your
// button. It's up to you. Note that the
// sprite is drawn behind the text.
//
// If you need a toggle like behaviour, set
// toggleButton to true and each time the button
// is clicked, the onToggled signal will be emitted.
// The toggled property determines if a toggle button
// is toggled or not. It's changed by toggle(),
// which is called each time the button is clicked.
///////////////////////////////////////
class Button : public Widget
{
public:
	/////////////////////////////////////
	// Constant values to define the internal
	// layout of Button components.
	/////////////////////////////////////
	enum Type
	{
		Normal,		// Button behind text in the same position
		Checkbox	// Text at left then Sprite spaced by Button::spaceBetween
	};

	/////////////////////////////////////
	// Constructs a sprite button
	//
	// Sets sprite's texture.
	/////////////////////////////////////
	Button(const std::string &texture = "", bool toggle = false, Type type = Normal);

	/////////////////////////////////////
	// Constructs a text button
	//
	// Sets the string, font and font size of the text.
	// Sets if the button is toggle like or not.
	/////////////////////////////////////
	Button(const std::string &text, const std::string &font, int fontSize = 26, bool toggle = false, Type type = Normal);

	/////////////////////////////////////
	// Constructs a text button
	//
	// Sets the string, font and font size of the text.
	// Sets if the button is toggle like or not.
	/////////////////////////////////////
	Button(const std::string &text, sf::Font *font, int fontSize = 26, bool toggle = false, Type type = Normal);

	/////////////////////////////////////
	// Constructs a sprite button with
	// text.
	//
	// Sets sprite's texture and string, font
	// and forn size of the text.
	/////////////////////////////////////
	Button(const std::string &texture, const std::string &text, const std::string &font, int fontSize = 26, bool toggle = false, Type type = Normal);

	/////////////////////////////////////
	// Constructs a sprite button with
	// text.
	//
	// Sets sprite's texture and string, font
	// and forn size of the text.
	/////////////////////////////////////
	Button(const std::string &texture, const std::string &text, sf::Font *font, int fontSize = 26, bool toggle = false, Type type = Normal);

	/////////////////////////////////////
	// Sets the type of the button
	/////////////////////////////////////
	void setType(Type type);

	/////////////////////////////////////
	// Gets the type of the button
	/////////////////////////////////////
	Type getType() const;

	/////////////////////////////////////
	// Sets the space between both components
	// in Checkbox type buttons in pixels
	/////////////////////////////////////
	void setSpaceBetween(int spaceBetween);

	/////////////////////////////////////
	// Gets the space between both components
	// in Checkbox type buttons in pixels
	/////////////////////////////////////
	int getSpaceBetween() const;

	/////////////////////////////////////
	/// Gets a const pointer to the texture
	/// of the sprite
	//
	// See Sprite::
	/////////////////////////////////////
	const sf::Texture * getTexture() const;

	/////////////////////////////////////
	// Gets the texture of the sprite
	//
	// See Sprite::getTexxture
	/////////////////////////////////////
	sf::Texture * getTexture();

	/////////////////////////////////////
	// Sets a texture to the sprite component
	//
	// See Sprite::setTexture
	/////////////////////////////////////
	void setTexture(const std::string &texturePath, bool resetRect = false);

	/////////////////////////////////////
	// Sets a texture to the sprite component
	//
	// See Sprite::setTexture
	/////////////////////////////////////
	void setTexture(sf::Texture *texture, bool resetRect = false);

	/////////////////////////////////////
	// Sets the area of the texture
	// which will be displayed by
	// the sprite component.
	//
	// See Sprite::setTextureRect
	/////////////////////////////////////
	void setTextureRect(const sf::IntRect &rect);

	/////////////////////////////////////
	// Shortcut for setString(), setFont()
	// and setFontSize(), is like
	// the constructor of the Text class ;)
	/////////////////////////////////////
	void setText(const sf::String &string, const std::string &fontPath, UInt fontSize = 26);

	/////////////////////////////////////
	// Shortcut for setString(), setFont()
	// and setFontSize(), is like
	// the constructor of the Text class ;)
	/////////////////////////////////////
	void setText(const sf::String &string, sf::Font *font, UInt fontSize = 26);

	/////////////////////////////////////
	// Gets the string of the text
	//
	// See Text::getString
	/////////////////////////////////////
	const sf::String & getString() const;

	/////////////////////////////////////
	// Sets the string of the text
	//
	// See Text::setString
	/////////////////////////////////////
	void setString(const sf::String &string);

	/////////////////////////////////////
	// Gets a const pointer to the font
	// of the text
	//
	// See Text::getFont
	/////////////////////////////////////
	const sf::Font * getFont() const;

	/////////////////////////////////////
	// Gets a pointer to the font of the text
	//
	// See Text::getFont
	/////////////////////////////////////
	sf::Font * getFont();

	/////////////////////////////////////
	// Loads a font, if it has not been
	// loaded yet, and sets it
	//
	// See Text::setFont
	/////////////////////////////////////
	void setFont(const std::string &fontPath);

	/////////////////////////////////////
	// Sets the font of the text
	//
	// See Text::setFont
	/////////////////////////////////////
	void setFont(sf::Font *font);

	/////////////////////////////////////
	// Tells if the text is bold
	//
	// See Text::isBold
	/////////////////////////////////////
	bool isBold() const;

	/////////////////////////////////////
	// Sets if the text should use the
	// bold version of its font
	//
	// See Text::setBold
	/////////////////////////////////////
	void setBold(bool bold);

	/////////////////////////////////////
	// Gets the size of the font used
	// by the text in pixels
	//
	// See Text::getFontSize
	/////////////////////////////////////
	UInt getFontSize() const;

	/////////////////////////////////////
	// Sets the size of the font used
	// by the text component in pixels
	//
	// See Text::setFontSize
	/////////////////////////////////////
	void setFontSize(UInt fontSize);

	/////////////////////////////////////
	// Gets the outline thickness of the
	// text
	//
	// See Text::getOutlineThickness
	/////////////////////////////////////
	float getOutlineThickness() const;

	/////////////////////////////////////
	// Sets the outline thickness of the text
	//
	// See Text::setOutlineThickness
	/////////////////////////////////////
	void setOutlineThickness(float thickness);

	/////////////////////////////////////
	// Gets the color of the text
	//
	// See Text::getColor
	/////////////////////////////////////
	const Color & getColor() const;

	/////////////////////////////////////
	/// Sets the color of the text and sprite
	/////////////////////////////////////
	void setColor(const Color &color);

	void setSpriteColor(const Color &color);
	void setTextColor(const Color &color);

	/////////////////////////////////////
	// Gets the outline's color of the text
	//
	// See Text::getOutlineColor
	/////////////////////////////////////
	const Color & getOutlineColor() const;

	/////////////////////////////////////
	// Sets the outline's color of the text
	//
	// See Text::setOutlineColor
	/////////////////////////////////////
	void setOutlineColor(const Color &color);

	/// Reimplemented Entity methods

	/////////////////////////////////////
	/// Sets a property using a XMLAttribute
	/////////////////////////////////////
	bool setProperty(const XMLAttribute &property) override;

	/////////////////////////////////////
	// Tells if the button is a toggle button
	/////////////////////////////////////
	bool isToggle() const;

	/////////////////////////////////////
	// Sets if the button must be a toggle
	// button.
	/////////////////////////////////////
	void setToggle(bool toggle);

	/////////////////////////////////////
	// Tells if the button is toggled.
	/////////////////////////////////////
	bool isToggled() const;

	/////////////////////////////////////
	// Change the toggled state of the button.
	//
	// Changes the value of toggled and emits
	// onToggled.
	//
	// **Note:** toggleButton must be true.
	/////////////////////////////////////
	void toggle();

	/////////////////////////////////////
	// Emitted when a toggle button is toggled.
	//
	// It's emited by toggle().
	/////////////////////////////////////
	Signal <> onToggled;

private:
	/////////////////////////////////////
	/// If the components need update,
	/// repositions them
	/////////////////////////////////////
	void onDraw(sf::RenderTarget &target, sf::RenderStates states) const override;

	/////////////////////////////////////
	// Defines how the components will be
	// positioned. Type::Normal by default.
	//
	// **Accessors:**
	//
	// + setType()
	// + getType()
	/////////////////////////////////////
	Type type;

	/////////////////////////////////////
	// Space between both components in pixels
	// in Type::Checkbox. 5 by default.
	//
	// **Accessors:**
	//
	// + setSpaceBetween()
	// + getSpaceBetween()
	/////////////////////////////////////
	int spaceBetween;

	/////////////////////////////////////
	// You can use it to represent the
	// button itself or the background of the
	// text.
	/////////////////////////////////////
	mutable Sprite *sprite;

	/////////////////////////////////////
	// You can use it to represent the button
	// itself or to draw text over the sprite
	// component.
	/////////////////////////////////////
	mutable Text *text;

	/////////////////////////////////////
	// Indicates if the components need to
	// be repositioned. *Dev only*
	//
	// It's true when the size of the components
	// change. Setting a new texture, change the
	// string of the text, etc.
	/////////////////////////////////////
	mutable bool componentsNeedUpdate;

	/////////////////////////////////////
	// Indicates if the button is toggle like.
	//
	// It's false by default but can be set
	// in the constructors or using setToggle()
	//
	// If it's true, toggle() will toggle
	// toggled value and emit onToggled. This
	// function is called each time the user clicks
	// the button.
	//
	// **Related functions:**
	//
	// + setToggle()
	// + isToggle()
	// + toggle()
	/////////////////////////////////////
	bool toggleButton;

	/////////////////////////////////////
	// Indicates if the button is toggled or not.
	//
	// It's false by default and is changed by
	// toggle() function only if the button is
	// toggle like (toggleButton is true).
	//
	// **Related functions:**
	//
	// + isToggled()
	// + toggle()
	/////////////////////////////////////
	bool toggled;
};

} // namespace lk

#endif // LITTLE_KNIGHT_BUTTON_HPP
