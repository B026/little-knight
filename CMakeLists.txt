cmake_minimum_required(VERSION 3.0)
project(little-knight)

set(CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/cmake)
set(CMAKE_INSTALL_PREFIX "/usr/")

# Looking for SFML
find_package(SFML COMPONENTS graphics window audio system REQUIRED)
include_directories(${SFML_INCLUDE_DIR})

# LittleKnight files
include_directories(${CMAKE_SOURCE_DIR}/include)
add_subdirectory(${CMAKE_SOURCE_DIR}/src/LK)
install(DIRECTORY ${CMAKE_SOURCE_DIR}/include/LK DESTINATION include COMPONENT dev)

export(EXPORT LKTargets FILE "${CMAKE_CURRENT_BINARY_DIR}/LK/LKTargets.cmake" NAMESPACE LK::)
configure_file(cmake/LKConfig.cmake "${CMAKE_CURRENT_BINARY_DIR}/LK/LKConfig.cmake")
configure_file(doc/doxen.conf.cmake "${CMAKE_SOURCE_DIR}/doc/doxen.conf")

set(ConfigPackageLocation lib/cmake/LK)
install(EXPORT LKTargets FILE LKTargets.cmake NAMESPACE LK:: DESTINATION ${ConfigPackageLocation})
install(FILES cmake/LKConfig.cmake DESTINATION ${ConfigPackageLocation} COMPONENT dev)
install(PROGRAMS tools/lk-squire.py DESTINATION bin)
