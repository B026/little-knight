/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/Scene.hpp>
#include <LK/Color.hpp>
#include <LK/Sprite.hpp>
#include <SFML/System/Clock.hpp>
#include <SFML/Window/Event.hpp>
#include <SFML/Window/Mouse.hpp>

namespace lk
{
/////////////////////////////////////
int Scene::defaultCamera = -1;

/////////////////////////////////////
Scene::Scene() : director(&Director::getInstance()), testCollisions(false), cameraIDGenerator(Scene::defaultCamera - 1), currentCamera(0),
				 timeoutIdGenerator(0)
{
	Vector2D directorSize(director->getSize().x, director->getSize().y);
	createCamera(directorSize * 0.5f, directorSize);
	setCamera(Scene::defaultCamera);
	addCamera(Scene::defaultCamera);

	cursorSprite = new Sprite;
	cursorSprite->setName("cursor-sprite");
	cursorSprite->setZIndex(100);
	addChild(cursorSprite);
}

/////////////////////////////////////
void Scene::setCursorTexture(const std::string &filename)
{
	cursorSprite->setTexture(filename);
	cursorSprite->setOriginCenter();
}

/////////////////////////////////////
void Scene::setBackgroundColor(const sf::Color &color)
{
	backgroundColor = color;
}

/////////////////////////////////////
void Scene::enableCollisions(bool testCollisions)
{
	this->testCollisions = testCollisions;
}

/////////////////////////////////////
void Scene::onAddChild(Entity *child)
{
	world.addEntity(child);
	child->onRelease.connect(std::bind(&World2D::removeEntity, &world, child));
}

/////////////////////////////////////
void Scene::onUpdate(float frameTime)
{
	const auto &mousePosition = sf::Mouse::getPosition();
	cursorSprite->setPosition(mousePosition.x, mousePosition.y);
}

/////////////////////////////////////
void Scene::dispatchEvents()
{
	sf::Event event;

	while (director->pollEvent(event))
	{
		for (auto &eventSignal : eventSignals)
		{
			if (event.type == eventSignal.eventType)
			{
				eventSignal.signal.emit(event);
				break;
			}
		}

		if (event.type == sf::Event::KeyPressed)
			for (auto &keyPressedSignal : keyPressedSignals)
				if (keyPressedSignal.key == event.key.code)
					keyPressedSignal.signal.emit();
	}
}

/////////////////////////////////////
DirectorAction & Scene::run()
{
	running = true;

	sf::Clock clock;
	while (running)
	{
		dispatchEvents();

		float frameTime = clock.restart().asSeconds();
		update(frameTime);

		for (auto &timeout : timeouts)
			timeout.update(frameTime);

		for (auto it = timeouts.begin(); it != timeouts.end();)
		{
			if (it->isDone())
				timeouts.erase(it);
			else
				++it;
		}

		if (testCollisions)
			world.update();

		for (auto &camera : cameras)
			camera.updatePosition();

		director->clear(backgroundColor);
		director->setView(*getCamera(currentCamera));
		director->draw(*this);
		world.draw(*director);
		director->display();
	}

	return directorAction;
}

/////////////////////////////////////
void Scene::close(int data)
{
	running = false;
	onClose(data);
}

/////////////////////////////////////
void Scene::onClose(int data)
{
}

/////////////////////////////////////
void Scene::onReopen()
{
}

/////////////////////////////////////
int Scene::connectToKeyPressed(sf::Keyboard::Key key, const std::function <void ()> &function, Entity *entity)
{
	for (auto &keyPressedSignal : keyPressedSignals)
		if (keyPressedSignal.key == key)
			return keyPressedSignal.signal.connect(function, entity);

	keyPressedSignals.emplace_back(key);
	return keyPressedSignals.back().signal.connect(function, entity);
}

/////////////////////////////////////
int Scene::connectToEvent(sf::Event::EventType eventType, const std::function <void (const sf::Event &)> &function, Entity *entity)
{
	for (auto &eventSignal : eventSignals)
		if (eventSignal.eventType == eventType)
			return eventSignal.signal.connect(function, entity);

	eventSignals.emplace_back(eventType);
	return eventSignals.back().signal.connect(function, entity);
}

/////////////////////////////////////
int Scene::setTimeout(const std::function <void()> &function, float limitTime, bool repeatable)
{
	timeouts.emplace_back(function, timeoutIdGenerator++, limitTime, repeatable);
	return timeoutIdGenerator - 1;
}

/////////////////////////////////////
void Scene::removeTimeout(int timeoutId)
{
	for (auto &timeout : timeouts)
	{
		if (timeout.getId() == timeoutId)
		{
			timeout.setDone();
			return;
		}
	}
}

/////////////////////////////////////
int Scene::createCamera(const Vector2D &cameraCenter, const Vector2D &cameraSize)
{
	cameras.emplace_back(cameraCenter, cameraSize, ++cameraIDGenerator);
	return cameraIDGenerator;
}

/////////////////////////////////////
Camera * Scene::getCamera(int cameraID)
{
	for (auto &camera : cameras)
		if (camera.getID() == cameraID)
			return &camera;

	return nullptr;
}

/////////////////////////////////////
void Scene::setCamera(int cameraID)
{
	if (currentCamera == cameraID)
		return;

	auto *camera = getCamera(cameraID);

	if (! camera)
		return;

	director->setView(*camera);
	currentCamera = cameraID;
}

} // namespace lk
