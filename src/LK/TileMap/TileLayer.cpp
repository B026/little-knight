/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/TileMap/TileLayer.hpp>
#include <LK/TileMap/TileMap.hpp>

namespace lk
{
/////////////////////////////////////
TileLayer::TileLayer(const std::string &name, const TileMap *tileMap, const Tileset *tileset) :
					 tileMap(tileMap), tileset(tileset), verticesNeedUpdate(true)
{
	setName(name);

	const auto &tileMapSize = tileMap->getDimensions();

	for (int column = 0; column < tileMapSize.x; column++)
		for (int row = 0; row < tileMapSize.y; row++)
			source.emplace_back(-1);


	vertices.setPrimitiveType(sf::Quads);
	vertices.resize(tileMapSize.x * tileMapSize.y * 4);
}

/////////////////////////////////////
void TileLayer::setSource(int source [])
{
	const auto &tileMapSize = tileMap->getDimensions();

	for (int row = 0; row < tileMapSize.y; row++)
	{
		for (int column = 0; column < tileMapSize.x; column++)
		{
			int index = row * tileMapSize.x + column;
			this->source [index] = source [index];
		}
	}

	verticesNeedUpdate = true;
}

/////////////////////////////////////
void TileLayer::reset(int value)
{
	const auto &tileMapSize = tileMap->getDimensions();

	for (int row = 0; row < tileMapSize.y; row++)
		for (int column = 0; column < tileMapSize.x; column++)
			this->source [row * tileMapSize.x + column] = value;

	verticesNeedUpdate = true;
}

/////////////////////////////////////
int TileLayer::get(const Point &tilePosition) const
{
	return get(tilePosition.x, tilePosition.y);
}

/////////////////////////////////////
int TileLayer::get(int x, int y) const
{
	return source [y * tileMap->getDimensions().x + x];
}

/////////////////////////////////////
Point TileLayer::getPositionOf(int value) const
{
	const auto &tileMapDimensions = tileMap->getDimensions();

	for (int row = 0; row < tileMapDimensions.y; row++)
		for (int column = 0; column < tileMapDimensions.x; column++)
			if (source [row * tileMapDimensions.x + column] == value)
				return Point(column, row);

	return Point(-1, -1);
}

/////////////////////////////////////
void TileLayer::set(const Point &tilePosition, int value)
{
	set(tilePosition.y * tileMap->getDimensions().x + tilePosition.x, value);
}

/////////////////////////////////////
void TileLayer::set(int x, int y, int value)
{
	set(y * tileMap->getDimensions().x + x, value);
}

/////////////////////////////////////
void TileLayer::set(int index, int value)
{
	source [index] = value;
	verticesNeedUpdate = true;
}

/////////////////////////////////////
void TileLayer::swap(const Point &tilePositionA, const Point &tilePositionB)
{
	int tileAtB = get(tilePositionB);
	set(tilePositionB, get(tilePositionA));
	set(tilePositionA, tileAtB);
}

/////////////////////////////////////
void TileLayer::updateVertices() const
{
	const auto &tileMapSize = tileMap->getDimensions();
	const auto &tileSize = tileMap->getTileSize();

	for (int row = 0; row < tileMapSize.y; row++)
	{
		for (int column = 0; column < tileMapSize.x; column++)
		{
			int index = row * tileMapSize.x + column;
			auto *tile = tileset->getTile(source [index]);
			sf::Vertex *quad = &vertices [index * 4];

			if (tile)
			{
				Vector2D quadTopLeftCornerPosition(column * tileSize.x, row * tileSize.y);
				quad [0].position = quadTopLeftCornerPosition;
				quad [1].position = quadTopLeftCornerPosition + Vector2D(0, + tileSize.y);
				quad [2].position = quadTopLeftCornerPosition + Vector2D(tileSize.x, + tileSize.y);
				quad [3].position = quadTopLeftCornerPosition + Vector2D(tileSize.x, 0);

				quad [0].texCoords = Vector2D(tile->topLeftTextureRect.x, tile->topLeftTextureRect.y);
				quad [1].texCoords = Vector2D(tile->topLeftTextureRect.x, tile->topLeftTextureRect.y + tileSize.y);
				quad [2].texCoords = Vector2D(tile->topLeftTextureRect.x + tileSize.x, tile->topLeftTextureRect.y + tileSize.y);
				quad [3].texCoords = Vector2D(tile->topLeftTextureRect.x + tileSize.x, tile->topLeftTextureRect.y);
			}
			else
			{
				quad [0].position = quad [1].position = quad [2].position = quad [3].position = Vector2D(0, 0);
			}
		}
	}

	verticesNeedUpdate = false;
}

/////////////////////////////////////
void TileLayer::onDraw(sf::RenderTarget &target, sf::RenderStates states) const
{
	if (verticesNeedUpdate)
		updateVertices();

	states.transform *= getTransform();
	states.texture = tileset->getTexture();
	target.draw(vertices, states);
}

} // namespace lk
