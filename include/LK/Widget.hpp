/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#ifndef LITTLE_KNIGHT_WIDGET_HPP
#define LITTLE_KNIGHT_WIDGET_HPP

#include <LK/Entity.hpp>
#include <LK/Scene.hpp>
#include <LK/Signal.hpp>

namespace lk
{

class Widget : public Entity
{
public:
	enum State
	{
		Normal, 	///< Normal
		Hover,		///< Under the mouse
		Pressed,	///< Mouse pressed over the widget
		Focus		///< Focused, after click and only if the widget is focusable
	};

	/////////////////////////////////////
	/// Default constructor
	///
	/// Passing the scene will allow to the
	/// widget process events
	/////////////////////////////////////
	Widget(bool focusable = false);

	/////////////////////////////////////
	/// If you did not pass the scene
	/// in the constructor, here, you can
	/// set it.
	/////////////////////////////////////
	void setScene(Scene *scene);

	/////////////////////////////////////
	/// Gets the actual state of the widget
	/////////////////////////////////////
	State getState() const;

	/////////////////////////////////////
	/// Mouse moved event processor
	/// You can reimplement this function
	/// to describe the behaviour
	/// of the widget when the mouse is moved
	/////////////////////////////////////
	virtual void mouseMovedEvent(const sf::Event &event);

	/////////////////////////////////////
	/// Mouse button pressed event processor
	/// You can reimplement this function
	/// to describe the behaviour
	/// of the widget when the mouse is pressed
	/////////////////////////////////////
	virtual void mouseButtonPressedEvent(const sf::Event &event);

	/////////////////////////////////////
	/// Mouse button pressed event processor
	/// You can reimplement this function
	/// to describe the behaviour
	/// of the widget when the mouse is released
	/////////////////////////////////////
	virtual void mouseButtonReleasedEvent(const sf::Event &event);

	/// Signals

	/////////////////////////////////////
	/// Emitted when the mouse is moved
	/// onto the widget.
	/////////////////////////////////////
	Signal <> onMouseOver;

	/////////////////////////////////////
	/// Emitted when the mouse is moved
	/// from the widget to out of the widget.
	/////////////////////////////////////
	Signal <> onMouseLeave;

	/////////////////////////////////////
	/// Emitted when the mouse's left button
	/// is pressed over the widget.
	/////////////////////////////////////
	Signal <> onMouseLeftPressed;

	/////////////////////////////////////
	/// Emitted when the mouse's right button
	/// is pressed over the widget.
	/////////////////////////////////////
	Signal <> onMouseRightPressed;

	/////////////////////////////////////
	/// Emitted when the mouse's left button
	/// is released over the widget
	/////////////////////////////////////
	Signal <> onClick;

	/////////////////////////////////////
	/// Emitted when the mouse's right button
	/// is released over the widget
	/////////////////////////////////////
	Signal <> onRightClick;

	/////////////////////////////////////
	/// Emitted when the widget gains the
	/// focus. This only happens with
	/// focusable widgets.
	/////////////////////////////////////
	Signal <> onFocus;

	/////////////////////////////////////
	/// Emitted when the widget losses the
	/// focus. This only happens with
	/// focusable widgets.
	/////////////////////////////////////
	Signal <> onBlur;

	/////////////////////////////////////
	/// Emitted when the state of the
	/// widget changes. Emits the new
	/// state of the widget
	/////////////////////////////////////
	Signal <State> onStateChanged;

protected:
	/////////////////////////////////////
	/// Sets the state of the widget
	/////////////////////////////////////
	void setState(State state);

private:
	/////////////////////////////////////
	/// The state of the widget
	/////////////////////////////////////
	State state;

	/////////////////////////////////////
	/// Tells if the widget is focusable or
	/// not (i.e., if it can gains the focus)
	/// This only can be set in the constructor
	/// False by default
	/////////////////////////////////////
	bool focusable;
};

} // namespace lk

#endif // LITTLE_KNIGHT_WIDGET_HPP
