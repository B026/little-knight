/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#ifndef LITTLE_KNIGHT_PROGRESS_BAR_HPP
#define LITTLE_KNIGHT_PROGRESS_BAR_HPP

#include <LK/AbstractBar.hpp>
#include <LK/Text.hpp>

namespace lk
{

class ProgressBar : public AbstractBar
{
public:
	enum TextFormat
	{
		Value,
		MinOverMax,
		Percentage
	};

	enum TextPosition
	{
		After,
		Before,
		Bottom,
		Middle,
		Top
	};

	ProgressBar();

	void setTextFormat(TextFormat format);
	TextFormat getTextFormat() const;

	void setTextPosition(TextPosition textPosition);
	TextPosition getTextPosition() const;

	void setMargin(float margin);
	float getMargin() const;

	void setFont(const std::string &filename);
	void setFont(sf::Font *font);
	void setFontSize(int fontSize);
	void setColor(const Color &color);
	void setOutlineThickness(float thickness);
	void setOutlineColor(const Color &color);

	/// Reimplemented Entity methods

	/////////////////////////////////////
	/// Sets a property using a XMLAttribute
	/////////////////////////////////////
	bool setProperty(const XMLAttribute &property) override;

private:
	void setTextNeedUpdate();
	void onDraw(sf::RenderTarget &target, sf::RenderStates states) const;

	TextFormat textFormat;
	TextPosition textPosition;
	float margin;
	mutable bool textNeedUpdate;
	mutable Text *text;
	Entity *backgroundSprite;
};

} // namespace lk

#endif // LITTLE_KNIGHT_PROGRESS_BAR_HPP
