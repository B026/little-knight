/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/TileMap/TileMap.hpp>
#include <LK/TileMap/TileLayer.hpp>
#include <LK/AssetsManager.hpp>
#include <LK/XMLNode.hpp>
#include <regex>

namespace lk
{
/////////////////////////////////////
TileMap::TileMap(const Point &dimensions, const Point &tileSize) : dimensions(dimensions), tileSize(tileSize)
{
}

/////////////////////////////////////
TileMap::TileMap(const std::string &textureFilename, const Point &tileSize, const std::string &tilesetName, const Point &dimensions) :
				 dimensions(dimensions), tileSize(tileSize)
{
	addTileset(textureFilename, tilesetName);
}

/////////////////////////////////////
TileMap::TileMap(const std::string &tmxFilename)
{
/*	XMLNode document(tmxFilename);
	std::regex tmxPathRegEx("(.*)\\/.*\\.tmx");
	std::smatch match;
	std::regex_match(tmxFilename, match, tmxPathRegEx);
	std::string tmxPath(match [1]);

	/// Read map data
	XMLNode *map = document.getChildByName("map");
	if (! map)
		return;

	dimensions.x = std::stoi(map->getAttribute("width"));
	dimensions.y = std::stoi(map->getAttribute("height"));
	tileSize.x = std::stoi(map->getAttribute("tilewidth"));
	tileSize.y = std::stoi(map->getAttribute("tileheight"));

	/// Tilesets
	const auto &tilesetNodes = map->getChildrenByName("tileset");
	for (auto *tilesetNode : tilesetNodes)
	{
		XMLNode tilesetDocument(tmxPath + "/" + tilesetNode->getAttribute("source"));
		addTileset(tmxPath + "/" + tilesetDocument.getChildByName("tileset")->getAttribute("source"), "",
				   std::stoi(tilesetNode->getAttribute("firstgid")));
	}

	// Layers
	const auto &layerNodes = map->getChildrenByName("layer");
	for (auto *layerNode : layerNodes)
	{
		auto layer = addLayer();
		const auto &source = layerNode->getChildByName("data")->getValue();
		auto it = source.begin();
		auto end = source.end();

		for (int x = 0; x < dimensions.x; x++)
		{
			for (int y = 0; y < dimensions.y; y++)
			{
				std::string number;

				while (! isdigit(*it))
					it++;

				while (isdigit(*it) && it != end)
					number.emplace_back (*it);


			}
		}
	}*/
}

/////////////////////////////////////
void TileMap::addTileset(sf::Texture *texture, const std::string &name, int tileIdGenerator)
{
	tilesets.emplace_back(texture, tileSize, name, tileIdGenerator);
}

/////////////////////////////////////
void TileMap::addTileset(const std::string &textureFilename, const std::string &name, int tileIdGenerator)
{
	addTileset(AssetsManager::getTexture(textureFilename), name, tileIdGenerator);
}

/////////////////////////////////////
const Tileset * TileMap::getTileset(const std::string &name) const
{
	for (auto &tileset : tilesets)
		if (tileset.getName() == name)
			return &tileset;

	return nullptr;
}

/////////////////////////////////////
const Point & TileMap::getDimensions() const
{
	return dimensions;
}

/////////////////////////////////////
const Point & TileMap::getTileSize() const
{
	return tileSize;
}

/////////////////////////////////////
Point TileMap::vectorToTilePosition(const Vector2D &vector)
{
	const auto &size = getSize();
	const auto &position = getPosition();
	const auto &topLeftCorner = position - size * 0.5f;
	const auto &localVector = vector - topLeftCorner;

	return Point(localVector.x / tileSize.x, localVector.y / tileSize.y);
}

/////////////////////////////////////
TileLayer * TileMap::addLayer(const std::string &tilesetName, const std::string &layerName)
{
	TileLayer *layer = new TileLayer(layerName, this, getTileset(tilesetName));
	addChild(layer);
	return layer;
}

/////////////////////////////////////
Vector2D TileMap::getSize() const
{
	return Vector2D(dimensions.x * tileSize.x, dimensions.y * tileSize.y);
}

} // namespace lk
