/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/Timeout.hpp>

namespace lk
{
/////////////////////////////////////
Timeout::Timeout(const std::function <void()> &function, int id, float limitTime, bool repeatable) :
				 function(function), id(id), limitTime(limitTime), repeatable(repeatable), done(false), currentTime(0)
{
}

/////////////////////////////////////
void Timeout::update(float frameTime)
{
	if (done)
		return;

	currentTime += frameTime;

	if (currentTime >= limitTime)
	{
		function();

		if (repeatable)
			currentTime -= limitTime;
		else
			done = true;
	}
}

/////////////////////////////////////
int Timeout::getId() const
{
	return id;
}

/////////////////////////////////////
bool Timeout::isDone() const
{
	return done;
}

/////////////////////////////////////
void Timeout::setDone()
{
	done = true;
}

} // namespace lk
