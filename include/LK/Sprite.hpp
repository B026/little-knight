/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#ifndef LITTLE_KNIGHT_SPRITE_HPP
#define LITTLE_KNIGHT_SPRITE_HPP

#include <LK/Entity.hpp>
#include <SFML/Graphics/Vertex.hpp>

namespace lk
{

///////////////////////////////////////
// Class used to draw textures.
//
// A sprite can be a spaceship, a bullet,
// an enemy, etc.
//
// A sprite draws a given texture (a .png image).
// It can draw the entire texture or just
// a small part of it (setTextureRect()).
///////////////////////////////////////
class Sprite : public Entity
{
public:
	///////////////////////////////////////
	// Constructs an empty sprite.
	///////////////////////////////////////
	Sprite();

	///////////////////////////////////////
	// Constructs a sprite and sets its texture.
	//
	// texturePath param is the filename of the
	// texture relative to AssetsManager::assetsPath
	///////////////////////////////////////
	Sprite(const std::string &texturePath);

	///////////////////////////////////////
	// Constructs a sprite and sets its texture.
	//
	// The texture param is a pointer to the
	// texture. It can be got by AssetsManager::getTexture()
	///////////////////////////////////////
	Sprite(sf::Texture *texture);

	///////////////////////////////////////
	// Gets a const pointer to the texture
	// of the sprite.
	//
	// Can return nullptr.
	///////////////////////////////////////
	const sf::Texture * getTexture() const;

	///////////////////////////////////////
	// Gets a pointer to the texture of the sprite.
	//
	// Can return nullptr.
	///////////////////////////////////////
	sf::Texture * getTexture();

	///////////////////////////////////////
	// Sets the texture of the sprite.
	//
	// **Params:**
	//
	// + *texturePath* is the texture's file name
	// relative to AssetsManager::assetsPath
	// + *resetRect* determines if the size of
	// the sprite and texture coords must be
	// recalculated using the new texture. If
	// the prev texture has a size of 32x32 and
	// you're setting a 64x64 texture, if
	// resetRect is false, the sprite will draw
	// only the 32x32 portion of the new texture,
	// if it's true, the sprite will draw the whole
	// texture.
	///////////////////////////////////////
	void setTexture(const std::string &texturePath, bool resetRect = false);

	///////////////////////////////////////
	// Sets the texture of the sprite.
	//
	// **Params:**
	//
	// + *texture* a pointer to the texture.
	// Can be got by AssetsManager::getTexture()
	// relative to AssetsManager::assetsPath
	// + *resetRect* determines if the size of
	// the sprite and texture coords must be
	// recalculated using the new texture. If
	// the prev texture has a size of 32x32 and
	// you're setting a 64x64 texture, if
	// resetRect is false, the sprite will draw
	// only the 32x32 portion of the new texture,
	// if it's true, the sprite will draw the whole
	// texture.
	///////////////////////////////////////
	void setTexture(sf::Texture *texture, bool resetRect = false);

	///////////////////////////////////////
	// Sets the area of the texture that the
	// sprite will draw.
	//
	// *textureRect* indicates the area of
	// the texture drawn by the sprite.
	///////////////////////////////////////
	void setTextureRect(const sf::IntRect &textureRect);

	/// Reimplemented Entity virtual methods

	///////////////////////////////////////
	// Colors the sprite.
	//
	// Applies the given color to the sprite.
	///////////////////////////////////////
	void setColor(const Color &color) override;

	///////////////////////////////////////
	// Gets the size of the sprite.
	///////////////////////////////////////
	Vector2D getSize() const override;

	///////////////////////////////////////
	///////////////////////////////////////
	bool setProperty(const XMLAttribute &property) override;

private:
	///////////////////////////////////////
	// Draws the sprite. *Dev only*
	//
	// It there's texture, draws the texture
	// over vertices
	///////////////////////////////////////
	void onDraw(sf::RenderTarget &target, sf::RenderStates states) const override;

	///////////////////////////////////////
	// The drawable object of the sprite. *Dev only*
	///////////////////////////////////////
	sf::Vertex vertices [4];

	///////////////////////////////////////
	// The texture drawn by the sprite.
	//
	// It's nullptr by default and can be set
	// in the constructor or using setTexture().
	// If it's nullptr, your sprite will never
	// be drawn.
	//
	// If you don't want to draw the entire texture
	// but just an area of it, use setTextureRect()
	// to set the area.
	//
	// **Related functions:**
	//
	// + setTexture()
	// + getTexture()
	// + setTextureRect()
	///////////////////////////////////////
	sf::Texture *texture;
};

} // namespace lk

#endif // LITTLE_KNIGHT_SPRITE_HPP
