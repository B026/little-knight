#!/usr/bin/python3
import sys
import os

if len(sys.argv) is 2:
	project_name = sys.argv [1]
else:
	project_name = input("Enter project's name: ")

if project_name is "--help" or project_name is "-h":
	print("This script can helps you to create a little knight project. Just run it ;)")
	sys.exit()

# Verify that folder doesn't exist
if os.path.exists(project_name):
	print("Folder already exists")
	sys.exit()

require_tile_map = input("Do you require tile map? [Y/N]: ")
if require_tile_map is 'Y' or require_tile_map is 'y':
	require_tile_map = True
else:
	require_tile_map = False

main_scene_class_name = input("Enter main scene class name: ")

# Create project's folders
os.mkdir(project_name)
os.mkdir(project_name + "/assets")
os.mkdir(project_name + "/build")
os.mkdir(project_name + "/include")
os.mkdir(project_name + "/src")

# CMakeLists.txt
file = open(project_name + "/CMakeLists.txt", "w")
file.write("cmake_minimum_required(VERSION 2.8)\n"
			"project(" + project_name + ")\n\n"
			"configure_file(Assets.h.cmake ${CMAKE_SOURCE_DIR}/include/Assets.h)\n"
			"include_directories(${CMAKE_SOURCE_DIR}/include)\n"
			"set(SRC src/main.cpp src/" + main_scene_class_name + ".cpp)\n\n"
			"add_executable(" + project_name + " ${SRC})\n"
			"target_link_libraries(" + project_name + " -llittle-knight -lsfml-window -lsfml-graphics -lsfml-system")

if require_tile_map:
	file.write(" -llittle-knight-tile-map)")
else:
	file.write(")");

file.close()

# Assets.h.cmake
file = open(project_name + "/Assets.h.cmake", "w")
file.write('#define ASSETS "${CMAKE_SOURCE_DIR}/assets/"')
file.close()

# MainScene.hpp
file = open(project_name + "/include/" + main_scene_class_name + ".hpp", "w")
file.write("#ifndef MAIN_SCEHE_HPP\n"
		   "#define MAIN_SCEHE_HPP\n\n"
		   "#include <LK/Scene.hpp>\n"
		   "using namespace lk;\n\n"
		   "class " + main_scene_class_name + " : public Scene\n"
		   "{\n"
		   "public:\n"
		   "	void onCreate() override;\n"
		   "	void onClose(int scene) override;\n"
		   "};\n\n"
		   "#endif // MAIN_SCEHE_HPP")
file.close()

# MainScene.cpp
file = open(project_name + "/src/" + main_scene_class_name + ".cpp", "w")
file.write('#include "' + main_scene_class_name + '.hpp"\n\n'
		   "void " + main_scene_class_name + "::onCreate()\n"
		   "{\n"
		   "	connectToKeyPressed(sf::Keyboard::Escape, std::bind(&Scene::close, this, 0));\n"
		   "}\n\n"
		   "void " + main_scene_class_name + "::onClose(int scene)\n"
		   "{\n"
		   "}\n\n")
file.close()

# main.cpp
file = open(project_name + "/src/main.cpp", "w")
file.write("#include <LK/AssetsManager.hpp>\n"
		   "#include <LK/Director.hpp>\n"
		   "using namespace lk;\n\n"
		   '#include "Assets.h"\n'
		   '#include "' + main_scene_class_name + '.hpp"\n\n'
		   "int main()\n"
		   "{\n"
		   "	AssetsManager::assetsPath = ASSETS;\n"
		   "	auto &director = Director::getInstance();\n"
		   '	director.create(sf::VideoMode::getDesktopMode(), "' + project_name + '");\n\n'
		   "	return director.startGame(new " + main_scene_class_name + ");\n"
		   "}\n")
file.close()
