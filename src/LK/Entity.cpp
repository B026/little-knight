/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/Entity.hpp>
#include <LK/Director.hpp>
#include <LK/SignalDef.inl>
#include <LK/Scene.hpp>
#include <stack>

namespace lk
{

/////////////////////////////////////
Entity::Entity() : visible(true), zIndex(0), parent(nullptr), collisionMask(0), needRelease(false)
{
	shape.setEntity(this);
	actions.setTarget(this);
}

/////////////////////////////////////
Entity::~Entity()
{
	for (auto *child : children)
		delete child;
}

/////////////////////////////////////
void Entity::release()
{
	needRelease = true;
}

/////////////////////////////////////
bool Entity::isReleased() const
{
	return needRelease;
}

/////////////////////////////////////
bool Entity::isVisible() const
{
	return visible;
}

/////////////////////////////////////
void Entity::setVisible(bool visible)
{
	this->visible = visible;
}

/////////////////////////////////////
int Entity::getZIndex() const
{
	return zIndex;
}

/////////////////////////////////////
void Entity::setZIndex(int zIndex)
{
	if (this->zIndex == zIndex)
		return;

	this->zIndex = zIndex;

	if (parent)
		parent->repositionChild(this);
}

/////////////////////////////////////
const std::string & Entity::getName() const
{
	return name;
}

/////////////////////////////////////
void Entity::setName(const std::string &name)
{
	this->name = name;
}

/////////////////////////////////////
bool Entity::addChild(Entity *entity, AddChildPolicy policy)
{
	if (! entity || entity->parent)
		return false;

	bool added = false;

	/// Try to add it before of other
	for (auto it = children.begin(); it != children.end(); ++it)
	{
		if (entity->zIndex < (*it)->zIndex)
		{
			entity->setParent(this);
			children.insert(it, entity);
			added = true;
			break;
		}
	}

	/// Append it
	if (! added)
	{
		entity->setParent(this);
		children.push_back(entity);
	}

	if (policy == AddCameras)
		for (auto &camera : cameras)
			entity->addCamera(camera);

	onAddChild(entity);
	return true;
}

/////////////////////////////////////
void Entity::onAddChild(Entity *child)
{
}

/////////////////////////////////////
bool Entity::removeChild(Entity *entity, bool release)
{
	if (! entity || entity->parent != this)
		return false;

	for (auto it = children.begin(); it != children.end(); ++it)
	{
		if (*it == entity)
		{
			onRemoveChild(entity);
			children.erase(it);
			entity->setParent(nullptr);
			if (release)
				entity->release();
			return true;
		}
	}

	return false;
}

/////////////////////////////////////
bool Entity::removeChildByName(const std::string &childName, bool release)
{
	for (auto it = children.begin(); it != children.end(); ++it)
	{
		if ((*it)->name == childName)
		{
			Entity *entity = *it;
			onRemoveChild(entity);
			children.erase(it);
			entity->setParent(nullptr);
			if (release)
				entity->release();
			return true;
		}
	}

	return false;
}

/////////////////////////////////////
void Entity::removeChildren(bool release)
{
	while (children.size())
		removeChild(children [0], release);	
}

/////////////////////////////////////
void Entity::onRemoveChild(Entity *child)
{
}

/////////////////////////////////////
const Entity * Entity::getChild(const std::string &childName) const
{
	return getChild(childName);
}

/////////////////////////////////////
Entity * Entity::getChild(const std::string &childName)
{
	for (auto *child : children)
		if (child->name == childName)
			return child;

	return nullptr;
}

/////////////////////////////////////
Entity * Entity::getChildByIndex(int index)
{
	if (index < 0 || index >= children.size())
		return nullptr;

	return children [index];
}

/////////////////////////////////////
const Entity * Entity::getChildByIndex(int index) const
{
	return getChildByIndex(index);
}

/////////////////////////////////////
const std::vector <Entity *> & Entity::getChildren()
{
	return children;
}

/////////////////////////////////////
const std::vector <Entity *> & Entity::getChildren() const
{
	return children;
}

/////////////////////////////////////
const Entity * Entity::getParent() const
{
	return parent;
}

/////////////////////////////////////
Entity * Entity::getParent()
{
	return parent;
}

/////////////////////////////////////
void Entity::update(float deltaTime)
{
	if (needRelease)
	{
		safeRelease();
		return;
	}

	onUpdate(deltaTime);
	actions.update(deltaTime);

	for (size_t i = 0; i < children.size(); ++i)
		children [i]->update(deltaTime);
}

/////////////////////////////////////
void Entity::onUpdate(float deltaTime)
{
}

/////////////////////////////////////
void Entity::setShape(const Shape &shape)
{
	this->shape = shape;
	this->shape.setEntity(this);
}

/////////////////////////////////////
const Shape & Entity::getShape() const
{
	return shape;
}

/////////////////////////////////////
Shape & Entity::getShape()
{
	return shape;
}

/////////////////////////////////////
void Entity::setCollisionMask(int collisionMask)
{
	this->collisionMask = collisionMask;
}

/////////////////////////////////////
int Entity::getCollisionMask() const
{
	return collisionMask;
}

/////////////////////////////////////
void Entity::onCollision(Collision &collision)
{
}

/////////////////////////////////////
void Entity::setColor(const Color &color)
{
}

/////////////////////////////////////
Vector2D Entity::getSize() const
{
	return Vector2D();
}

/////////////////////////////////////
void Entity::setOriginCenter()
{
	setAnchorPoint(0.5, 0.5);
}

/////////////////////////////////////
void Entity::setAnchorPoint(float x, float y)
{
	const auto &size = getSize();
	setOrigin(size.x * x, size.y * y);
}

/////////////////////////////////////
void Entity::setAnchorPoint(const Vector2D &anchorPoint)
{
	setAnchorPoint(anchorPoint.x, anchorPoint.y);
}

/////////////////////////////////////
sf::Transform Entity::getAncestorsTransform() const
{
    std::stack <const Entity *> parents;
    auto *parent = getParent();

    while (parent)
    {
        parents.push(parent);
        parent = parent->getParent();
    }

    sf::Transform transform;
    while (parents.size())
    {
        transform.combine(parents.top()->getTransform());
        parents.pop();
    }

    return transform;
}

/////////////////////////////////////
sf::Transform Entity::getFullTransform() const
{
	return getAncestorsTransform().combine(getTransform());
}

/////////////////////////////////////
sf::FloatRect Entity::getBoundingBox() const
{
    if (! visible)
        return getFullTransform().transformRect(sf::FloatRect());

    const auto &size = getSize();
    return getFullTransform().transformRect(sf::FloatRect(0, 0, size.x, size.y));
}

/////////////////////////////////////////////
sf::FloatRect Entity::getFullBoundingBox() const
{
    auto fullBox = getBoundingBox();
	float right = fullBox.left + fullBox.width;
	float bottom = fullBox.top + fullBox.height;

    if (visible)
    {
        for (auto *child : children)
        {
            const auto &childBox = child->getFullBoundingBox();
			float childRight = childBox.left + childBox.width;
			float childBottom = childBox.top + childBox.height;
			right = std::max(right, childRight);
			bottom = std::max(bottom, childBottom);

            fullBox.left = std::min(fullBox.left, childBox.left);
            fullBox.top = std::min(fullBox.top, childBox.top);
            fullBox.width = right - fullBox.left;
            fullBox.height = bottom - fullBox.top;
        }
    }

    return fullBox;
}

/////////////////////////////////////
void Entity::addCamera(int newCamera, bool addToChildren)
{
	/// Verify that the cameras has not been added yet
	for (auto &camera : cameras)
		if (camera == newCamera)
			return;

	/// Add it
	cameras.push_back(newCamera);

	if (addToChildren)
		for (auto *child : children)
			child->addCamera(newCamera, addToChildren);
}

/////////////////////////////////////
void Entity::removeCamera(int camera, bool removeFromChildren)
{
	bool removed = false;

	for (auto it = cameras.begin(); it != cameras.end(); ++it)
	{
		if (*it == camera)
		{
			cameras.erase(it);
			removed = true;
			break;
		}
	}

	if (removed && removeFromChildren)
		for (auto *child : children)
			child->removeCamera(camera, removeFromChildren);
}

/////////////////////////////////////
void Entity::setProperties(const std::vector <XMLAttribute> &properties)
{
	for (auto &property : properties)
		setProperty(property);
}

/////////////////////////////////////
bool Entity::setProperty(const XMLAttribute &property)
{
	if (property.name == "name")
	{
		setName(property.value);
		return true;
	}

	if (property.name == "z-index")
	{
		setZIndex(stoi(property.value));
		return true;
	}

	if (property.name == "visible")
	{
		setVisible(property.value == "true");
		return true;
	}

	if (property.name == "position")
	{
		setPosition(Vector2D(property.value));
		return true;
	}

	if (property.name == "origin")
	{
		if (property.value == "center")
			setOriginCenter();
		else
			setOrigin(Vector2D(property.value));

		return true;
	}

	return false;
}

/////////////////////////////////////
void Entity::setData(const std::string &name, const std::string &value)
{
	data.set(name, value);
}

/////////////////////////////////////
const std::string & Entity::getData(const std::string &name)
{
	return data.getValue(name);
}

/////////////////////////////////////
const JSON & Entity::getJSONData() const
{
	return data;
}

/////////////////////////////////////
void Entity::safeRelease()
{
	for (auto *child : children)
	{
		child->setParent(nullptr);
		child->release();
	}

	if (parent)
		parent->removeChild(this);

	onRelease.emit(this);
	delete this;
}

/////////////////////////////////////
void Entity::setParent(Entity *parent)
{
	this->parent = parent;
}

/////////////////////////////////////
void Entity::repositionChild(Entity *child)
{
	removeChild(child);
	addChild(child, NoAddCameras);
}

/////////////////////////////////////
void Entity::draw(sf::RenderTarget &target, sf::RenderStates states) const
{
	if (needRelease || ! visible)
		return;

	states.transform *= getTransform();

	auto *scene = Director::getScene();

	for (auto &camera : cameras)
	{
		scene->setCamera(camera);
		onDraw(target, states);
	}

	for (auto *child : children)
		child->draw(target, states);
}

/////////////////////////////////////
void Entity::onDraw(sf::RenderTarget &target, sf::RenderStates states) const
{
}

} // namespace lk
