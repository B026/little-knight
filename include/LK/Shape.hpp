/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#ifndef LITTLE_KNIGHT_SHAPE_HPP
#define LITTLE_KNIGHT_SHAPE_HPP

#include <LK/Projection.hpp>
#include <LK/Vector2D.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <vector>

namespace lk
{

class Entity;

class Shape
{
public:
	Shape();

	void setEntity(Entity *entity);

	void pushVertex(float vertexX, float vertexY);
	void pushVertex(const Vector2D &vertex);
	int getVertexCount() const;
	std::vector <Vector2D> getTransformedVertices() const;

	virtual std::vector <Vector2D> getAxes() const;
	void getAxes(std::vector <Vector2D> &axes) const;
	Projection getProjection(const Vector2D &axis) const;

	void draw(sf::RenderTarget &target);

	static Shape boxShape(const Vector2D &size);

private:
	std::vector <Vector2D> vertices;
	Entity *entity;
};

} // namespace lk

#endif // LITTLE_KNIGHT_SHAPE_HPP
