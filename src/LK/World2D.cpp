/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/World2D.hpp>
#include <LK/SignalDef.inl>
#include <cmath>

namespace lk
{
/////////////////////////////////////
void World2D::addEntity(Entity *entity)
{
	entities.emplace_back(entity);
	entity->onRelease.connect(std::bind(&World2D::removeEntity, this, entity));
}

/////////////////////////////////////
void World2D::removeEntity(Entity *entity)
{
	for (auto it = entities.begin(); it != entities.end(); ++it)
	{
		if (*it == entity)
		{
			entities.erase(it);
			return;
		}
	}
}

/////////////////////////////////////
void World2D::update()
{
	for (int i = 0; i < entities.size() - 1; i++)
	{
		auto *entityA = entities [i];

		if (! entityA->isReleased())
		{
			for (int j = i + 1; j < entities.size(); j++)
			{
				auto *entityB = entities [j];

				if (! entityA->isReleased() && ! entityB->isReleased() && entityA->getCollisionMask() != entityB->getCollisionMask())
					testSAT(entityA, entityB);
			}
		}
	}
}

/////////////////////////////////////
void World2D::testSAT(Entity *entityA, Entity *entityB)
{
	const auto &shapeA = entityA->getShape();
	const auto &shapeB = entityB->getShape();

	if (! shapeA.getVertexCount() || ! shapeB.getVertexCount())
		return;

	/// Get the axes of the shapes
	std::vector <Vector2D> axes;
	shapeA.getAxes(axes);
	shapeB.getAxes(axes);

	/// Check if they're colliding
	Vector2D mtv(10000, 10000); // Minimum translation vector
	for (auto &axis : axes)
	{
		const auto &projectionA = shapeA.getProjection(axis);
		const auto &projectionB = shapeB.getProjection(axis);

		if (! projectionA.overlaps(projectionB))
			return;

		float overlap = projectionA.getOverlap(projectionB);
		if (abs(overlap) < mtv.getLength())
			mtv = axis * overlap;
	}

	Collision collisionA(entityB, mtv);
	entityA->onCollision(collisionA);

	if (collisionA.solved)
		return;

	Collision collisionB(entityA, mtv);
	entityB->onCollision(collisionB);

	if (! collisionB.solved)
		entityA->move(mtv);
}

void World2D::draw(sf::RenderTarget &target)
{
	/*for (auto *entity : entities)
		entity->getShape().draw(target);*/
}

} // namespace lk
