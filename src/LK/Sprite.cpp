/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/Sprite.hpp>
#include <LK/AssetsManager.hpp>

namespace lk
{
/////////////////////////////////////
Sprite::Sprite() : texture(nullptr)
{
}

/////////////////////////////////////
Sprite::Sprite(const std::string &texturePath) : texture(nullptr)
{
	setTexture(texturePath);
}

/////////////////////////////////////
Sprite::Sprite(sf::Texture *texture) : texture(nullptr)
{
	setTexture(texture);
}

/////////////////////////////////////
const sf::Texture * Sprite::getTexture() const
{
	return texture;
}

/////////////////////////////////////
sf::Texture * Sprite::getTexture()
{
	return texture;
}

/////////////////////////////////////
void Sprite::setTexture(const std::string &texturePath, bool resetRect)
{
	setTexture(AssetsManager::getTexture(texturePath), resetRect);
}

/////////////////////////////////////
void Sprite::setTexture(sf::Texture *texture, bool resetRect)
{
	if (! texture || (this->texture == texture && ! resetRect))
		return;

	if (! this->texture || resetRect)
	{
        this->texture = texture;
        const auto &textureSize = texture->getSize();
        setTextureRect(sf::IntRect(0, 0, textureSize.x, textureSize.y));
        return;
	}

	this->texture = texture;
}

/////////////////////////////////////
void Sprite::setTextureRect(const sf::IntRect &textureRect)
{
	int textureRectBottom = textureRect.top + textureRect.height;
	int textureRectRight = textureRect.left + textureRect.width;

    vertices [0].position = sf::Vector2f(0, 0);
    vertices [0].texCoords = sf::Vector2f(textureRect.left, textureRect.top);
    vertices [1].position = sf::Vector2f(0, textureRect.height);
    vertices [1].texCoords = sf::Vector2f(textureRect.left, textureRectBottom);
    vertices [2].position = sf::Vector2f(textureRect.width, 0);
    vertices [2].texCoords = sf::Vector2f(textureRectRight, textureRect.top);
    vertices [3].position = sf::Vector2f(textureRect.width, textureRect.height);
    vertices [3].texCoords = sf::Vector2f(textureRectRight, textureRectBottom);
}

/////////////////////////////////////
void Sprite::setColor(const Color &color)
{
	for (int i = 0; i < 4; i++)
		vertices [i].color = color;
}

/////////////////////////////////////
Vector2D Sprite::getSize() const
{
	return vertices [3].position;
}

/////////////////////////////////////
bool Sprite::setProperty(const XMLAttribute &property)
{
	if (property.name == "texture")
	{
		setTexture(property.value);
		return true;
	}

	return Entity::setProperty(property);
}

/////////////////////////////////////
void Sprite::onDraw(sf::RenderTarget &target, sf::RenderStates states) const
{
    if (! texture)
        return;

    states.texture = texture;
    target.draw(vertices, 4, sf::TriangleStrip, states);
}

} // namespace lk
