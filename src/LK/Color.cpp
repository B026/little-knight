/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/Color.hpp>

namespace lk
{
/////////////////////////////////////
Color::Color()
{
}

/////////////////////////////////////
Color::Color(const std::string &string)
{
	if (string.size() && string [0] == '#')
	{
		if (string.size() != 4 && string.size() != 7)
			r = g = b = 0;

		if (string.size() == 4)
		{
			const auto &redString = string.substr(1, 1);
			const auto &greenString = string.substr(2, 1);
			const auto &blueString = string.substr(3, 1);
			r = std::stoi(redString + redString, nullptr, 16);
			g = std::stoi(greenString + greenString, nullptr, 16);
			b = std::stoi(blueString + blueString, nullptr, 16);
		}
		else
		{
			r = std::stoi(string.substr(1, 2), nullptr, 16);
			g = std::stoi(string.substr(3, 2), nullptr, 16);
			b = std::stoi(string.substr(5, 2), nullptr, 16);
		}
	}
}

/////////////////////////////////////
Color::Color(const sf::Color &color) : sf::Color(color)
{
}

} // namespace lk
