/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#ifndef LITTLE_KNIGHT_SCENE_HPP
#define LITTLE_KNIGHT_SCENE_HPP

#include <LK/Camera.hpp>
#include <LK/Director.hpp>
#include <LK/Entity.hpp>
#include <LK/Signal.hpp>
#include <LK/Timeout.hpp>
#include <LK/World2D.hpp>
#include <SFML/Graphics/Color.hpp>
#include <SFML/Window/Keyboard.hpp>
#include <SFML/Window/Event.hpp>
#include <vector>

namespace lk
{

class Sprite;

class Scene : public Entity
{
public:
	Scene();

	void setCursorTexture(const std::string &filename);
	void setBackgroundColor(const sf::Color &color);
	void enableCollisions(bool testCollisions);

	/// Loop methods

	virtual void onCreate() = 0;
	void dispatchEvents();
	DirectorAction & run();
	void close(int data);
	virtual void onClose(int data);
	virtual void onReopen();

	/// Signal methods

	int connectToKeyPressed(sf::Keyboard::Key key, const std::function <void ()> &function, Entity *entity = nullptr);
	int connectToEvent(sf::Event::EventType eventType, const std::function <void (const sf::Event &)> &, Entity *entity = nullptr);

	int setTimeout(const std::function <void()> &function, float limitTime, bool repeatable = false);
	void removeTimeout(int timeoutId);

	/// Cameras related methods

	int createCamera(const Vector2D &cameraCenter, const Vector2D &cameraSize);
	Camera * getCamera(int cameraID);
	void setCamera(int cameraID);

	/// Reimplemented methods from Entity

	void onAddChild(Entity *child) override;
	void onUpdate(float deltaTime) override;

	/// Public attributes

	Director *director;

	static int defaultCamera;

protected:
	DirectorAction directorAction;

private:
	bool running;
	sf::Color backgroundColor;

	struct KeyPressedSignal
	{
		Signal <> signal;
		sf::Keyboard::Key key;

		KeyPressedSignal(sf::Keyboard::Key key) : key(key) {}
	};

	std::vector <KeyPressedSignal> keyPressedSignals;

	struct EventSignal
	{
		Signal <const sf::Event &> signal;
		sf::Event::EventType eventType;

		EventSignal(sf::Event::EventType eventType) : eventType(eventType) {}
	};

	std::vector <EventSignal> eventSignals;
	std::vector <Timeout> timeouts;
	int timeoutIdGenerator;

	bool testCollisions;
	World2D world;

	std::vector <Camera> cameras;
	int currentCamera;
	int cameraIDGenerator;

	Sprite *cursorSprite;
};

} // namespace lk

#endif // LITTLE_KNIGHT_SCENE_HPP
