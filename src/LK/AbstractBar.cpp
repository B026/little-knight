/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/AbstractBar.hpp>
#include <LK/AssetsManager.hpp>

namespace lk
{
/////////////////////////////////////
AbstractBar::AbstractBar(bool focusable) : Widget(focusable), maxValue(maxValue), value(value), orientation(orientation), componentsNeedUpdate(true)
{
	backgroundSprite = new Sprite;
	backgroundSprite->setName("backgroundSprite");
	addChild(backgroundSprite);

	fillingSprite = new Sprite;
	fillingSprite->setName("fillingSprite");
	addChild(fillingSprite);
}

/////////////////////////////////////
AbstractBar::AbstractBar(const std::string &backgroundTexture, const std::string &fillingTexture, int maxValue, int value, Orientation orientation,
						 bool focusable) : Widget(focusable), maxValue(maxValue), value(value), orientation(orientation), componentsNeedUpdate(true)
{
	backgroundSprite = new Sprite;
	backgroundSprite->setName("backgroundSprite");
	addChild(backgroundSprite);
	setBackgroundTexture(backgroundTexture);

	fillingSprite = new Sprite;
	fillingSprite->setName("fillingSprite");
	addChild(fillingSprite);
	setFillingTexture(fillingTexture);

	updatePixelsPerValue();
}

/////////////////////////////////////
void AbstractBar::setValue(int value)
{
	if (this->value == value)
		return;

	if (value < 0)
		value = 0;

	if (value > maxValue)
		value = maxValue;

	this->value = value;
	componentsNeedUpdate = true;
	onValueChange.emit(value);
}

/////////////////////////////////////
int AbstractBar::getValue() const
{
	return value;
}

/////////////////////////////////////
void AbstractBar::setMaxValue(int maxValue)
{
	if (this->maxValue == maxValue)
		return;

	if (maxValue < 0)
		maxValue *= -1;

	this->maxValue = maxValue;
	componentsNeedUpdate = true;
	updatePixelsPerValue();
}

/////////////////////////////////////
int AbstractBar::getMaxValue() const
{
	return maxValue;
}

/////////////////////////////////////
void AbstractBar::setOrientation(Orientation orientation)
{
	if (this->orientation == orientation)
		return;

	this->orientation = orientation;
	componentsNeedUpdate = true;
}

/////////////////////////////////////
AbstractBar::Orientation AbstractBar::getOrientation() const
{
	return orientation;
}

/////////////////////////////////////
void AbstractBar::setBackgroundTexture(const std::string &backgroundTexture)
{
	setBackgroundTexture(AssetsManager::getTexture(backgroundTexture));
}

/////////////////////////////////////
void AbstractBar::setBackgroundTexture(sf::Texture *backgroundTexture)
{
	if (backgroundTexture == backgroundSprite->getTexture())
		return;

	backgroundSprite->setTexture(backgroundTexture);
	backgroundSprite->setOriginCenter();
	componentsNeedUpdate = true;
}

/////////////////////////////////////
void AbstractBar::setFillingTexture(const std::string &fillingTexture)
{
	setFillingTexture(AssetsManager::getTexture(fillingTexture));
}

/////////////////////////////////////
void AbstractBar::setFillingTexture(sf::Texture *fillingTexture)
{
	if (fillingTexture == fillingSprite->getTexture())
		return;

	fillingSprite->setTexture(fillingTexture);
	fillingSprite->setOriginCenter();
	componentsNeedUpdate = true;
	updatePixelsPerValue();
}

/////////////////////////////////////
bool AbstractBar::setProperty(const XMLAttribute &property)
{
	if (Entity::setProperty(property))
		return true;

	if (property.name == "value")
	{
		setValue(std::stoi(property.value));
		return true;
	}

	if (property.name == "max-value")
	{
		setMaxValue(std::stoi(property.value));
		return true;
	}

	if (property.name == "orientation")
	{
		setOrientation((property.value == "horizontal") ? Horizontal : Vertical);
		return true;
	}

	if (property.name.find("background-") != std::string::npos)
		return backgroundSprite->setProperty(property);

	if (property.name.find("filling-") != std::string::npos)
		return fillingSprite->setProperty(property);

	return false;
}

/////////////////////////////////////
void AbstractBar::updatePixelsPerValue()
{
	auto *fillingTexture = fillingSprite->getTexture();

	if (fillingTexture)
		pixelsPerValue = (float) fillingTexture->getSize().x / (float) maxValue;
}

/////////////////////////////////////
void AbstractBar::onDraw(sf::RenderTarget &target, sf::RenderStates states) const
{
	if (! componentsNeedUpdate)
		return;

	backgroundSprite->setOriginCenter();
	fillingSprite->setTextureRect(sf::IntRect(0, 0, pixelsPerValue * value, fillingSprite->getSize().y));
	componentsNeedUpdate = false;

	if (orientation == Horizontal)
	{
		backgroundSprite->setRotation(0);
		fillingSprite->setRotation(0);
	}
	else
	{
		backgroundSprite->setRotation(-90);
		fillingSprite->setRotation(-90);
	}
}

} // namespace lk
