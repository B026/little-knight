/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/Text.hpp>
#include <LK/Color.hpp>

namespace lk
{
/////////////////////////////////////
Text::Text() : font(nullptr), verticesNeedUpdate(true), colorHasChange(true), outlineColorHasChange(true),
               fontSize(26U), outlineThickness(0.f), color(sf::Color::White), bold(false)
{
    vertices.setPrimitiveType(sf::Triangles);
    outlineVertices.setPrimitiveType(sf::Triangles);
}

/////////////////////////////////////
Text::Text(const sf::String &string, sf::Font *font, UInt fontSize) :
           string (string), fontSize (fontSize), font (font), verticesNeedUpdate (true), colorHasChange (true),
           outlineColorHasChange (true), outlineThickness(0.f), color(sf::Color::White), bold(false)
{
    vertices.setPrimitiveType(sf::Triangles);
    outlineVertices.setPrimitiveType(sf::Triangles);
}

/////////////////////////////////////
Text::Text(const sf::String &string, const std::string &filename, UInt fontSize) :
           string(string), fontSize(fontSize), font(nullptr), verticesNeedUpdate(true), colorHasChange(true),
           outlineColorHasChange(true), outlineThickness(0.f), color(sf::Color::White), bold(false)
{
    vertices.setPrimitiveType(sf::Triangles);
    outlineVertices.setPrimitiveType(sf::Triangles);
    setFont(filename);
}

/////////////////////////////////////
void Text::setFont(sf::Font *font)
{
    if (this->font == font)
        return;

    this->font = font;
    verticesNeedUpdate = true;
}

/////////////////////////////////////
void Text::setFont(const std::string &fontFilename)
{
    setFont(AssetsManager::getFont(fontFilename));
}

/////////////////////////////////////
const sf::Font * Text::getFont() const
{
    return font;
}

/////////////////////////////////////
sf::Font * Text::getFont()
{
    return font;
}

/////////////////////////////////////
void Text::setFontSize(UInt fontSize)
{
    if (this->fontSize == fontSize)
        return;

    this->fontSize = fontSize;
    verticesNeedUpdate = true;
}

/////////////////////////////////////
UInt Text::getFontSize() const
{
    return fontSize;
}

/////////////////////////////////////
void Text::setOutlineThickness(float outlineThickness)
{
    if (this->outlineThickness == outlineThickness)
        return;

    this->outlineThickness = outlineThickness;
    verticesNeedUpdate = true;
}

/////////////////////////////////////
float Text::getOutlineThickness() const
{
    return outlineThickness;
}

/////////////////////////////////////
void Text::setBold(bool bold)
{
    if (this->bold == bold)
        return;

    this->bold = bold;
    verticesNeedUpdate = true;
}

/////////////////////////////////////
bool Text::isBold() const
{
    return bold;
}

/////////////////////////////////////
void Text::setString(const sf::String &string)
{
    if (this->string == string)
        return;

    this->string = string;
    verticesNeedUpdate = true;
}

/////////////////////////////////////
const sf::String & Text::getString() const
{
    return string;
}

/////////////////////////////////////
void Text::setColor(const Color &color)
{
    if (this->color == color)
        return;

    this->color = color;
    colorHasChange = true;
}

/////////////////////////////////////
const Color & Text::getColor() const
{
    return color;
}

/////////////////////////////////////
void Text::setOutlineColor(const Color &color)
{
    if (this->outlineColor == color)
        return;

    this->outlineColor = color;
    outlineColorHasChange = true;
}

/////////////////////////////////////
const Color & Text::getOutlineColor() const
{
    return outlineColor;
}

/////////////////////////////////////
Vector2D Text::getSize() const
{
	if (verticesNeedUpdate)
		updateVertices();

	return size;
}

/////////////////////////////////////
bool Text::setProperty(const XMLAttribute &property)
{
	if (property.name == "string")
	{
		setString(property.value);
		return true;
	}

	if (property.name == "font")
	{
		setFont(property.value);
		return true;
	}

	if (property.name == "font-size")
	{
		setFontSize(std::stoi(property.value));
		return true;
	}

	if (property.name == "color")
	{
		setColor(Color(property.value));
		return true;
	}

	if (property.name == "outline-thickness")
	{
		setOutlineThickness(std::stof(property.value));
		return true;
	}

	if (property.name == "outline-color")
	{
		setOutlineColor(Color(property.value));
		return true;
	}

	if (property.name == "bold")
	{
		setBold(property.value == "true");
		return true;
	}

	return Entity::setProperty(property);
}

/////////////////////////////////////
void Text::updateVertices() const
{
	verticesNeedUpdate = false;

	vertices.clear();
	outlineVertices.clear();

	// Neither font nor string; nothing to draw
	if (! font || string == "" || ! fontSize)
    {
        size.x = size.y = 0;
		return;
    }

    UInt prevChar = 0;
	float maxX = 0, maxY = 0;
	float hspace = font->getGlyph(' ', fontSize, bold).advance; // For ' ' and '\t'
	float vspace = font->getLineSpacing(fontSize); // For '\n'
    float maxTop = font->getGlyph('A', fontSize, bold).bounds.top;
	sf::Vector2f position(0, 0);

	for (UInt currentChar : string)
	{
		position.x += font->getKerning(prevChar, currentChar, fontSize);
		prevChar = currentChar;

		// Move the position for special characters.
		switch (currentChar)
		{
            case ' ':
                pushEmptyGlyph(vertices, position, sf::Vector2f(hspace, 0));
                position.x += hspace;
            break;

			case '\t': position.x += hspace * 4.f; break;
			case '\n': position.y += vspace; position.x = 0;
		}

		// Add the current char to the drawable object if is not a special character.
		if (currentChar != ' ' && currentChar != '\n' && currentChar != '\t')
		{
			// Add the outlined glyph and calculate the boundingBox values.
			if (outlineThickness > 0.f)
			{
				const auto &glyph = font->getGlyph(currentChar, fontSize, bold, outlineThickness);
				pushGlyph(outlineVertices, glyph, position, outlineColor, maxTop, outlineThickness);

    	        maxX = std::max(maxX, position.x + (float) glyph.bounds.width  - outlineThickness);
    	        maxY = std::max(maxY, position.y + (float) glyph.bounds.height - outlineThickness);
			}

			// Add the normal glyph
			const auto &glyph = font->getGlyph(currentChar, fontSize, bold);
			float t = pushGlyph(vertices, glyph, position, color, maxTop);

			// If the aabb values have not been calculated, calculate them.
			if (! outlineThickness)
			{
    	        maxX = std::max(maxX, position.x + glyph.bounds.width + glyph.bounds.left);
    	        maxY = std::max(maxY, position.y + glyph.bounds.height + t);
			}

            if (! position.x)
			    position.x += glyph.advance - glyph.bounds.left;
            else
                position.x += glyph.advance;
		}
	}

    float minTop = vertices [0].position.y;
    if (minTop > 0)
        for (int i = 0; i < vertices.getVertexCount(); i++)
            vertices [i].position.y -= minTop;

    if (outlineThickness)
    {
        minTop = outlineVertices [0].position.y;
        if (minTop > 0)
            for (int i = 0; i < outlineVertices.getVertexCount(); i++)
                outlineVertices [i].position.y -= minTop;
    }

    size.x = maxX;
    size.y = maxY;
}

/////////////////////////////////////
void Text::applyColor() const
{
    for (int i = 0; i < vertices.getVertexCount(); i++)
		vertices [i].color = color;

    colorHasChange = false;
}

/////////////////////////////////////
void Text::applyOutlineColor() const
{
    for (int i = 0; i < outlineVertices.getVertexCount(); i++)
        outlineVertices [i].color = outlineColor;

    outlineColorHasChange = false;
}

/////////////////////////////////////
void Text::onDraw(sf::RenderTarget &target, sf::RenderStates states) const
{
    if (! font || string.isEmpty())
        return;

    if (verticesNeedUpdate)     updateVertices();
    if (colorHasChange)         applyColor();
    if (outlineColorHasChange)  applyOutlineColor();

    states.texture = &font->getTexture(fontSize);
    if (outlineThickness > 0.f)
        target.draw(outlineVertices, states);
    target.draw(vertices, states);
}

/////////////////////////////////////////
void pushEmptyGlyph(sf::VertexArray &vertices, const sf::Vector2f &position, const sf::Vector2f &size)
{
    vertices.append(sf::Vertex(sf::Vector2f(position.x, position.y), Color::Transparent));
    vertices.append(sf::Vertex(sf::Vector2f(position.x + size.x, position.y), Color::Transparent));
    vertices.append(sf::Vertex(sf::Vector2f(position.x, position.y + size.y), Color::Transparent));
    vertices.append(sf::Vertex(sf::Vector2f(position.x, position.y + size.y), Color::Transparent));
    vertices.append(sf::Vertex(sf::Vector2f(position.x + size.x, position.y), Color::Transparent));
    vertices.append(sf::Vertex(sf::Vector2f(position.x + size.x, position.y + size.y), Color::Transparent));
}

/////////////////////////////////////////
float pushGlyph(sf::VertexArray &vertices, const sf::Glyph &glyph, const sf::Vector2f &position, const Color &color,
				float maxTop, float outlineThickness)
{
	float left      = glyph.bounds.left,
		  top       = glyph.bounds.top,
		  right     = glyph.bounds.width + left,
		  bottom    = glyph.bounds.height + top,
		  texLeft   = glyph.textureRect.left,
		  texRight  = glyph.textureRect.left + glyph.textureRect.width,
		  texTop    = glyph.textureRect.top,
		  texBottom = glyph.textureRect.top  + glyph.textureRect.height;

    if (! position.x)
    {
        right -= left;
        left = 0;
    }

    if (top == maxTop)
    {
        bottom -= top;
        top = 0;
    }
    else
    {
        top = - 1.f * (maxTop - top);
        bottom = glyph.bounds.height + top;
    }

	vertices.append (sf::Vertex (sf::Vector2f (position.x + left - outlineThickness, position.y + top - outlineThickness),
								 color, sf::Vector2f (texLeft, texTop)));
	vertices.append (sf::Vertex (sf::Vector2f (position.x + right - outlineThickness, position.y + top - outlineThickness),
								 color, sf::Vector2f (texRight, texTop)));
	vertices.append (sf::Vertex (sf::Vector2f (position.x + left - outlineThickness, position.y + bottom - outlineThickness),
								 color, sf::Vector2f (texLeft, texBottom)));
	vertices.append (sf::Vertex (sf::Vector2f (position.x + left - outlineThickness, position.y + bottom - outlineThickness),
								 color, sf::Vector2f (texLeft, texBottom)));
	vertices.append (sf::Vertex (sf::Vector2f (position.x + right - outlineThickness, position.y + top - outlineThickness),
								 color, sf::Vector2f (texRight, texTop)));
	vertices.append (sf::Vertex (sf::Vector2f (position.x + right - outlineThickness, position.y + bottom - outlineThickness),
								 color, sf::Vector2f (texRight, texBottom)));

    return top;
}

} // namespace lk
