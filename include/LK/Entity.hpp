/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#ifndef LITTLE_KNIGHT_ENTITY_HPP
#define LITTLE_KNIGHT_ENTITY_HPP

#include <LK/SignalDef.hpp>
#include <LK/Color.hpp>
#include <LK/XMLNode.hpp>
#include <LK/Collision.hpp>
#include <LK/JSON.hpp>
#include <LK/Shape.hpp>
#include <LK/ActionList.hpp>
#include <SFML/Graphics/Drawable.hpp>
#include <SFML/Graphics/RenderStates.hpp>
#include <SFML/Graphics/RenderTarget.hpp>
#include <SFML/Graphics/Transformable.hpp>
#include <vector>
#include <string>

namespace lk
{

class Entity : public sf::Drawable, public sf::Transformable
{
public:
	enum AddChildPolicy
	{
		AddCameras,
		NoAddCameras
	};

	Entity();
	virtual ~Entity();

	void release();
	bool isReleased() const;

	bool isVisible() const;
	void setVisible(bool visible);

	int getZIndex() const;
	void setZIndex(int zIndex);

	const std::string & getName() const;
	void setName(const std::string &name);

	bool addChild(Entity *entity, AddChildPolicy policy = AddCameras);
	virtual void onAddChild(Entity *child);
	bool removeChild(Entity *entity, bool release = true);
	bool removeChildByName(const std::string &childName, bool release = true);
	void removeChildren(bool release = true);
	virtual void onRemoveChild(Entity *child);
	Entity * getChildByIndex(int index);
	const Entity * getChildByIndex(int index) const;
	const Entity * getChild(const std::string &childName) const;
	Entity * getChild(const std::string &childName);
	const std::vector <Entity *> & getChildren();
	const std::vector <Entity *> & getChildren() const;

	const Entity * getParent() const;
	Entity * getParent();

	void update(float deltaTime);
	virtual void onUpdate(float deltaTime);

	void setShape(const Shape &shape);
	Shape & getShape();
	const Shape & getShape() const;

	void setCollisionMask(int collisionMask);
	int getCollisionMask() const;
	virtual void onCollision(Collision &collision);

	virtual void setColor(const Color &color);

	virtual Vector2D getSize() const;
	void setAnchorPoint(float x, float y);
	void setAnchorPoint(const Vector2D &anchorPoint);
	void setOriginCenter();
	sf::Transform getAncestorsTransform() const;
	sf::Transform getFullTransform() const;
	sf::FloatRect getBoundingBox() const;
	sf::FloatRect getFullBoundingBox() const;

	void addCamera(int camera, bool addToChildren = true);
	void removeCamera(int camera, bool removeFromChildren = true);

	void setProperties(const std::vector <XMLAttribute> &properties);
	virtual bool setProperty(const XMLAttribute &property);

	void setData(const std::string &name, const std::string &value);
	const std::string & getData(const std::string &name);
	const JSON & getJSONData() const;

	Signal <Entity *> onRelease;

	ActionList actions;

private:
	void safeRelease();

	void setParent(Entity *parent);
	void repositionChild(Entity *child);

	void draw(sf::RenderTarget &target, sf::RenderStates states) const override;
	virtual void onDraw(sf::RenderTarget &target, sf::RenderStates states) const;

	// Properties

	bool visible;
	int zIndex;
	std::string name;
	bool needRelease;
	JSON data;

	Entity *parent;
	std::vector <Entity *> children;

	Shape shape;
	int collisionMask;

	std::vector <int> cameras;
};

} // namespace lk

#endif // LITTLE_KNIGHT_ENTITY_HPP
