/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#ifndef LITTLE_KNIGHT_ASSETS_MANAGER_HPP
#define LITTLE_KNIGHT_ASSETS_MANAGER_HPP

#include <LK/SpriteAnimation.hpp>
#include <SFML/Graphics/Font.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Audio/SoundBuffer.hpp>
#include <vector>
#include <string>

namespace lk
{

///////////////////////////////////////
// Static class that holds all your game assets
// like textures, sounds, fonts, etc.
//
// It lets you load assets and delete them
// when you don't need them anymore.
//
// You can avoid using this class because
// the offered entities offer functions where
// they deal with it, but you can still using it.
// For example, if you a texture which is used
// by only 1 sprite, you can use the Sprite::setTexture()
// function which receives the texture filename,
// but if you have a texture which is used by
// 20 sprites, you should get it using getTexture(),
// store it temporally and then pass it to your
// 20 sprites using the Sprite::setTexture() function
// that receives a pointer to a texture.
//
// Each asset is identified by its full path
// in disk, so, the first time you get it,
// it will be loaded and then returned, the
// rest of times it just will be searched in
// the asset arrays and returned.
//
// The main purpose is that all your entities
// share common assets to speed up your game
// and to reduce the expense of memory.
///////////////////////////////////////
class AssetsManager
{
public:
	///////////////////////////////////////
	// Gets a pointer to a texture by a given
	// filename. It must be a .png
	///////////////////////////////////////
	static sf::Texture * getTexture(const std::string &texturePath);

	///////////////////////////////////////
	// Deletes a previously loaded texture
	///////////////////////////////////////
	static void deleteTexture(const std::string &texturePath);

	///////////////////////////////////////
	// Deletes all the loaded textures
	///////////////////////////////////////
	static void deleteTextures();

	///////////////////////////////////////
	// Gets a pointer to a font by a given
	// filename. See [here](https://www.sfml-dev.org/documentation/2.5.0/classsf_1_1Font.php#ab020052ef4e01f6c749a85571c0f3fd1)
	// for supported formats.
	///////////////////////////////////////
	static sf::Font * getFont(const std::string &fontPath);
	static void deleteFont(const std::string &fontPath);
	static void deleteFonts();

	static sf::SoundBuffer * getSound(const std::string &filaname);
	static void deleteSound(const std::string &filename);
	static void deleteSounds();

	static SpriteAnimation * loadSpriteAnimation(const std::string &spriteAnimationName, const std::string &texturePath,
												 const Vector2D &frameSize, int numberOfFrames = -1, int row = 0, int column = 0);
	static SpriteAnimation * loadSpriteAnimation(const std::string &spriteAnimationName, sf::Texture *texture, const Vector2D &frameSize,
												 int numberOfFrames = -1, int row = 0, int column = 0);
	static SpriteAnimation * getSpriteAnimation(const std::string &spriteAnimationName);
	static void deleteSpriteAnimation(const std::string &spriteAnimationName);
	static void deleteSpriteAnimations();

	static std::string assetsPath;

private:
	AssetsManager();
	~AssetsManager();

	static AssetsManager & getInstance();

	template <typename AssetType>
	struct Asset
	{
		std::string path;
		AssetType asset;

		Asset(const std::string &path) : path(path) {}
	};

	std::vector <Asset <sf::Texture> *> textures;
	std::vector <Asset <sf::Font> *> fonts;
	std::vector <Asset <sf::SoundBuffer> *> sounds;
	std::vector <SpriteAnimation *> spriteAnimations;
};

} // namespace lk

#endif // LITTLE_KNIGHT_ASSETS_MANAGER_HPP
