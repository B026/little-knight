/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#ifndef LITTLE_KNIGHT_TEXT_HPP
#define LITTLE_KNIGHT_TEXT_HPP

#include <LK/AssetsManager.hpp>
#include <LK/Entity.hpp>
#include <SFML/Graphics/VertexArray.hpp>

namespace lk
{

typedef unsigned int UInt;

///////////////////////////////////////
// Class used to draw text.
//
// A text draws a string using a given
// font with a determined size (fontSize).
// The text use color to fill the characters
// to draw. Also can use the bold version
// of the font to draw the string.
//
// If you want the text has outline, set its
// thickness (outlineThickness) and a specific
// color (outlineColor) to draw it.
///////////////////////////////////////
class Text : public Entity
{
public:
	///////////////////////////////////////
	// Constructs an empty text.
	///////////////////////////////////////
	Text();

	///////////////////////////////////////
	// Constructs a text with the most important
	// properties.
	//
	// Sets the string of the text, the font used to draw
	// it and the size of the font (fontSize) used.
	//
	// The font param is a pointer to the font
	// used which can be got by using AssetsManager::getFont()
	///////////////////////////////////////
	Text(const sf::String &string, sf::Font *font, UInt fontSize = 26U);

	///////////////////////////////////////
	// Constructs a text with the most important
	// properties.
	//
	// Sets the string of the text, the font used to draw
	// it and the size of the font (fontSize) used.
	//
	// The fontFilename param is the file name
	// of the font relative to AssetsManager::assetsPath
	///////////////////////////////////////
	Text(const sf::String &string, const std::string &fontFilename, UInt fontSize = 26U);

	///////////////////////////////////////
	// Sets the font used to draw the text.
	//
	// The param is a pointer to a font
	// which can be got by using AssetsManager::getFont()
	///////////////////////////////////////
	void setFont(sf::Font *font);

	///////////////////////////////////////
	// Sets the font used to draw the text.
	//
	// The param is the file name of the font
	// relative to AssetsManager::assetsPath
	///////////////////////////////////////
	void setFont(const std::string &filename);

	///////////////////////////////////////
	// Gets a const pointer to the font used
	// to draw the text.
	///////////////////////////////////////
	const sf::Font * getFont() const;

	///////////////////////////////////////
	// Gets a pointer to the font used
	// to draw the text.
	///////////////////////////////////////
	sf::Font * getFont();

	///////////////////////////////////////
	// Sets the size of the font used to
	// draw the text in pixels.
	//
	// **See** fontSize
	///////////////////////////////////////
	void setFontSize(UInt fontSize);

	///////////////////////////////////////
	// Gets the size of the font used to
	// draw the text in pixels.
	//
	// **See** fontSize
	///////////////////////////////////////
	UInt getFontSize() const;

	///////////////////////////////////////
	// Sets the thickness of the outline in
	// pixels.
	//
	// **See** outlineThickness
	///////////////////////////////////////
	void setOutlineThickness(float outlineThickness);

	///////////////////////////////////////
	// Gets the thickness of the outline in
	// pixels.
	//
	// **See** outlineThickness
	///////////////////////////////////////
	float getOutlineThickness() const;

	///////////////////////////////////////
	// Sets if the bold version of the font
	// must be used to draw the text.
	///////////////////////////////////////
	void setBold(bool bold);

	///////////////////////////////////////
	// Gets if the bold version of the font
	// must be used to draw the text.
	///////////////////////////////////////
	bool isBold() const;

	///////////////////////////////////////
	// Sets the string of the text.
	///////////////////////////////////////
	void setString(const sf::String &string);

	///////////////////////////////////////
	// Gets the string of the text.
	///////////////////////////////////////
	const sf::String & getString() const;

	///////////////////////////////////////
	// Sets the fill color of the text.
	///////////////////////////////////////
	void setColor(const Color &color) override;

	///////////////////////////////////////
	// Gets the fill color of the text.
	///////////////////////////////////////
	const Color & getColor() const;

	///////////////////////////////////////
	// Sets the outline color of the text.
	//
	// **See** outlineColor
	///////////////////////////////////////
	void setOutlineColor(const Color &color);

	///////////////////////////////////////
	// Gets the outline color of the text.
	//
	// **See** outlineColor
	///////////////////////////////////////
	const Color & getOutlineColor() const;

	///////////////////////////////////////
	// Gets the size of the text in pixels
	///////////////////////////////////////
	Vector2D getSize() const override;

	bool setProperty(const XMLAttribute &property) override;

private:
	///////////////////////////////////////
	// Updates the vertices and outlineVertices. *Dev only*
	//
	// Sets verticesNeedUpdate to false, calculates
	// the size of the text, and updates the drawable
	// elements of the text.
	//
	// This is the heaviest function of the class
	// and it's called only once by frame when
	// verticesNeedUpdate is true.
	///////////////////////////////////////
	void updateVertices() const;

	///////////////////////////////////////
	// Applies fill color to the vertices. *Dev only*
	//
	// It's called when colorHasChange is true
	// and sets it to false.
	///////////////////////////////////////
	void applyColor() const;

	///////////////////////////////////////
	// Applies outlineColor to the vertices. *Dev only*
	//
	// It's called when outlineColorHasChange is true
	// and sets it to false.
	///////////////////////////////////////
	void applyOutlineColor() const;

	///////////////////////////////////////
	// Draws the text. *Dev only*
	//
	// Calls to updateVertices(), applyColor()
	// and applyOutlineColor() when needed, and
	// draws vertices and outlineVertices.
	///////////////////////////////////////
	void onDraw(sf::RenderTarget &target, sf::RenderStates states) const override;

	///////////////////////////////////////
	// The size of the font used to draw
	// the text in pixels.
	//
	// It's 26 by default and can be set in
	// the constructors or using its set
	// function.
	//
	// **Related functions:**
	//
	// + getFontSize()
	// + setFontSize()
	///////////////////////////////////////
	UInt fontSize;

	///////////////////////////////////////
	// The thickness of the outline in pixels.
	//
	// It's 0 by default. 0 says that the text
	// doesn't have outline, so even if you sets
	// color to the outline (outlineColor)
	// it won't be drawn.
	//
	// **Related functions:**
	//
	// + getOutlineThickness()
	// + setOutlineThickness()
	///////////////////////////////////////
	float outlineThickness;

	///////////////////////////////////////
	// Tells if the text must be drawn used
	// the bold version of the font.
	//
	// It's false by default and even if you
	// set it to true, the usage of the bold
	// version depends on the font.
	//
	// **Related functions:**
	//
	// + isBold()
	// + setBold()
	///////////////////////////////////////
	bool bold;

	///////////////////////////////////////
	// The string to be drawn.
	//
	// It's empty by default but you can change it
	// in the constructors or using its set function.
	// If it's empty, your text won't be drawn
	// because there's nothing to draw.
	//
	// **Related functions:**
	//
	// + getString()
	// + setString()
	///////////////////////////////////////
	sf::String string;

	///////////////////////////////////////
	// The font used to drawn the text.
	//
	// It's nullptr by default and you can
	// specify it in the constructors or set it
	// using its set functions. If it's nullptr,
	// your text won't be drawn.
	//
	// There are 2 functions to set it, the difference
	// is that one receives a pointer to a [sf::Font](https://www.sfml-dev.org/documentation/2.5.0/classsf_1_1Font.php)
	// object which can be got using AssetsManager::getFont(),
	// and the other one receives the file name of your font
	// relative to AssetsManager::assetsPath.
	//
	// **Related functions:**
	//
	// + getFont()
	// + setFont()
	///////////////////////////////////////
	sf::Font *font;

	///////////////////////////////////////
	// The color used to color the text.
	//
	// It's white by default and you can change it
	// by using its set function.
	//
	// **Related functions:**
	//
	// + getColor()
	// + setColor()
	///////////////////////////////////////
	Color color;

	///////////////////////////////////////
	// The color used to color the text's outline.
	//
	// It's black by default and you can change it
	// by using its set function.
	//
	// **Note**: even if you set it, be sure that
	// the thickness of the outline (outlineThickness)
	// is higher that 0.
	//
	// **Related functions:**
	//
	// + getOutlineColor()
	// + setOutlineColor()
	///////////////////////////////////////
	Color outlineColor;

	///////////////////////////////////////
	// The size of the text in pixels. *Dev only*
	//
	// It's calculated in updateVertices()
	// and returned by getSize()
	///////////////////////////////////////
	mutable Vector2D size;

	///////////////////////////////////////
	// The drawable object of the text. *Dev only*
	//
	// It's updated in updateVertices()
	///////////////////////////////////////
	mutable sf::VertexArray vertices;

	///////////////////////////////////////
	// The drawable object of the outline. *Dev only*
	//
	// It's update in updateVertices() but
	// only if outlineThickness is higher than 0
	///////////////////////////////////////
	mutable sf::VertexArray outlineVertices;

	///////////////////////////////////////
	// Tells if the vertices need to be updated. *Dev only*
	//
	// It's true by default and it's set to false
	// by updateVertices(). It's set to true
	// each time string, font, fontSize or outlineThickness
	// are changed. Guarantees regardless the number of
	// times they're changed the updateVertices()
	// function is called just one by frame only if
	// if it's needed.
	//
	// If it's true, updateVertices() is called
	// by onDraw() before to draw the vertices.
	///////////////////////////////////////
	mutable bool verticesNeedUpdate;

	///////////////////////////////////////
	// Tells if the fill color has changed. *Dev only*
	//
	// True by default, set to false by applyColor()
	// and to true by setColor(). It's used
	// to guarantee that applyColor() is called
	// just once by frame when it's required.
	///////////////////////////////////////
	mutable bool colorHasChange;

	///////////////////////////////////////
	// Tells if the outlineColor has changed. *Dev only*
	//
	// True by default, set to false by applyOutlineColor()
	// and to true by setOutlineColor(). It's used
	// to guarantee that applyOutlineColor() is called
	// just once by frame when it's required.
	///////////////////////////////////////
	mutable bool outlineColorHasChange;
};

/////////////////////////////////////
// Function used by the text class
/////////////////////////////////////
void pushEmptyGlyph(sf::VertexArray &vertices, const sf::Vector2f &position, const sf::Vector2f &size);

/////////////////////////////////////
// Function used by the text class
/////////////////////////////////////
float pushGlyph(sf::VertexArray &vertices, const sf::Glyph &glyph, const sf::Vector2f &position, const Color &color,
				float maxTop, float outlineThickness = 0.f);

} // namespace lk

#endif // LITTLE_KNIGHT_TEXT_HPP
