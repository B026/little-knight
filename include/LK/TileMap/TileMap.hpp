/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#ifndef LITTLE_KNIGHT_TILE_MAP_TILE_MAP_HPP
#define LITTLE_KNIGHT_TILE_MAP_TILE_MAP_HPP

#include <LK/Entity.hpp>
#include <LK/Point.hpp>
#include <LK/TileMap/Tileset.hpp>
#include <SFML/Graphics/Texture.hpp>

namespace lk
{

class TileLayer;

class TileMap : public Entity
{
public:
	TileMap(const Point &dimensions = Point(), const Point &tileSize = Point());
	TileMap(const std::string &tilesetFilename, const Point &tileSize, const std::string &tilesetName, const Point &dimensions);
	TileMap(const std::string &tmxFilename);

	const Point & getDimensions() const;
	const Point & getTileSize() const;
	Point vectorToTilePosition(const Vector2D &vector);
	void addTileset(sf::Texture *texture, const std::string &name, int tileIdGenerator = 0);
	void addTileset(const std::string &textureFilename, const std::string &name, int tileIdGenerator = 0);
	const Tileset * getTileset(const std::string &name) const;
	TileLayer * addLayer(const std::string &tilesetName, const std::string &layerName = "");
	Vector2D getSize() const override;

private:
	Point dimensions;
	Point tileSize;
	std::vector <Tileset> tilesets;
};

} // namespace lk

#endif // LITTLE_KNIGHT_TILE_MAP_TILE_MAP_HPP
