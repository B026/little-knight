/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/ProgressBar.hpp>

namespace lk
{
/////////////////////////////////////
ProgressBar::ProgressBar() : AbstractBar(false), textNeedUpdate(true), textPosition(After), margin(5), textFormat(Value)
{
	text = new Text;
	text->setName("text");
	addChild(text);

	backgroundSprite = getChild("backgroundSprite");

	onValueChange.connect(std::bind(&ProgressBar::setTextNeedUpdate, this));
}

/////////////////////////////////////
void ProgressBar::setTextFormat(TextFormat format)
{
	if (textFormat == format)
		return;

	textFormat = format;
	setTextNeedUpdate();
}

/////////////////////////////////////
ProgressBar::TextFormat ProgressBar::getTextFormat() const
{
	return textFormat;
}

/////////////////////////////////////
void ProgressBar::setTextPosition(TextPosition textPosition)
{
	if (this->textPosition == textPosition)
		return;

	this->textPosition = textPosition;
	setTextNeedUpdate();
}

/////////////////////////////////////
ProgressBar::TextPosition ProgressBar::getTextPosition() const
{
	return textPosition;
}

/////////////////////////////////////
void ProgressBar::setMargin(float margin)
{
	if (this->margin == margin)
		return;

	this->margin = margin;
	setTextNeedUpdate();
}

/////////////////////////////////////
float ProgressBar::getMargin() const
{
	return margin;
}

/////////////////////////////////////
void ProgressBar::setFont(const std::string &filename)
{
	setFont(AssetsManager::getFont(filename));
}

/////////////////////////////////////
void ProgressBar::setFont(sf::Font *font)
{
	text->setFont(font);
	setTextNeedUpdate();
}

/////////////////////////////////////
void ProgressBar::setFontSize(int fontSize)
{
	text->setFontSize(fontSize);
	setTextNeedUpdate();
}

/////////////////////////////////////
void ProgressBar::setColor(const Color &color)
{
	text->setColor(color);
}

/////////////////////////////////////
void ProgressBar::setOutlineThickness(float thickness)
{
	text->setOutlineThickness(thickness);
	setTextNeedUpdate();
}

/////////////////////////////////////
void ProgressBar::setOutlineColor(const Color &color)
{
	text->setOutlineColor(color);
}

/////////////////////////////////////
void ProgressBar::setTextNeedUpdate()
{
	textNeedUpdate = true;
}

/////////////////////////////////////
bool ProgressBar::setProperty(const XMLAttribute &property)
{
	if (property.name == "text-format")
	{
		if (property.value == "MinOverMax")		 setTextFormat(MinOverMax);
		else if (property.value == "Percentage") setTextFormat(Percentage);
		else if (property.value == "Value")		 setTextFormat(Value);

		return true;
	}

	if (property.name == "text-position")
	{
		if (property.value == "After") 		 setTextPosition(After);
		else if (property.value == "Before") setTextPosition(Before);
		else if (property.value == "Bottom") setTextPosition(Bottom);
		else if (property.value == "Middle") setTextPosition(Middle);
		else if (property.value == "Top") 	 setTextPosition(Top);

		return true;
	}

	if (property.name == "margin")
	{
		setMargin(std::stof(property.value));
		return true;
	}

	return AbstractBar::setProperty(property) || text->setProperty(property);
}

/////////////////////////////////////
void ProgressBar::onDraw(sf::RenderTarget &target, sf::RenderStates states) const
{
	AbstractBar::onDraw(target, states);

	if (! textNeedUpdate)
		return;

	switch (textFormat)
	{
		case Value:		 text->setString(std::to_string(getValue())); break;
		case MinOverMax: text->setString(std::to_string(getValue()) + "/" + std::to_string(getMaxValue())); break;
		case Percentage: text->setString(std::to_string(getValue() * 100 / getMaxValue()) + "%");
	}

	text->setOriginCenter();
	const auto &textSize = text->getSize();
	const auto &backgroundSize = backgroundSprite->getSize();

	switch (textPosition)
	{
		case After:  text->setPosition(backgroundSize.x * 0.5f + margin + textSize.x * 0.5f, 0); break;
		case Before: text->setPosition(- (backgroundSize.x * 0.5f + margin + textSize.x * 0.5f), 0); break;
		case Middle: text->setPosition(0, 0); break;
		case Top:    text->setPosition(0, - (backgroundSize.y * 0.5f + margin + textSize.y * 0.5f)); break;
		case Bottom: text->setPosition(0, backgroundSize.y * 0.5f + margin + textSize.y * 0.5f);
	}

	textNeedUpdate = false;
}

} // namespace lk
