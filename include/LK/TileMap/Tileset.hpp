/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#ifndef LITTLE_KNIGHT_TILE_MAP_TILESET_HPP
#define LITTLE_KNIGHT_TILE_MAP_TILESET_HPP

#include <LK/Point.hpp>
#include <LK/TileMap/Tile.hpp>
#include <SFML/Graphics/Texture.hpp>
#include <string>
#include <vector>

namespace lk
{

class Tileset
{
public:
	Tileset(const std::string &name = "", int tileIdGenerator = 0);
	Tileset(sf::Texture *texture, const Point &tileSize, const std::string &name, int tileIdGenerator = 0);
	Tileset(const std::string &textureFilename, const Point &tileSize, const std::string &name, int tileIdGenerator = 0);
	void load(const std::string &textureFilename, const Point &tileSize);
	void load(sf::Texture *texture, const Point &tileSize);
	const std::string & getName() const;
	void setName(const std::string &name);
	const Tile * getTile(int tileId) const;
	const Point & getTileSize() const;
	const sf::Texture * getTexture() const;

public:
	sf::Texture *texture;
	int tileIdGenerator;
	Point tileSize;
	std::string name;
	std::vector <Tile> tiles;
};

} // namespace lk

#endif // LITTLE_KNIGHT_TILE_MAP_TILESET_HPP
