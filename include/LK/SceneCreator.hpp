/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#ifndef LITTLE_KNIGHT_SCENE_CREATOR_HPP
#define LITTLE_KNIGHT_SCENE_CREATOR_HPP

#include <LK/Entity.hpp>
#include <LK/XMLNode.hpp>

namespace lk
{

/////////////////////////////////////
/// Static class which helps to create
/// entities specified in a XML file
/// or XMLNode
/////////////////////////////////////
class SceneCreator
{
public:
	/////////////////////////////////////
	/// Creates the entities specified
	/// in the xml file which is in AssetsManager::assetsPath +
	/// filePath.
	///
	/// Adds them to entity.
	/////////////////////////////////////
	static void create(const std::string &filePath, Entity *entity);

	/////////////////////////////////////
	/// Creates the entities specified
	/// in xmlNode.
	/// Adds them to entity.
	/////////////////////////////////////
	static void create(const XMLNode &xmlNode, Entity *entity);
};

} // namespace lk

#endif // LITTLE_KNIGHT_SCENE_CREATOR_HPP
