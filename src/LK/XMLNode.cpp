/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/XMLNode.hpp>
#include <LK/AssetsManager.hpp>
#include <fstream>
#include <iostream>
#include <cctype>

namespace lk
{
///////////////////////////////////
std::string XMLNode::emptyString;

///////////////////////////////////
XMLNode::XMLNode(XMLNode *parent, const std::string &name) : parent(parent), name(name)
{
}

///////////////////////////////////
XMLNode::XMLNode(const std::string &filename) : parent(nullptr)
{
	/// Possible states of the XML reader
	enum State
	{
		Normal,
		LowerThan,
		ReadingAttributeName,
		ReadingAttributeValue,
		ReadingNodeCloseTag,
		ReadingNodeName,
		ReadingTextNode,
		WaitingForEqual,
		WaitingForGreaterThan,
		WaitingForQuotationMark
	};

	/// Read the content of the XML file
	std::ifstream inputStream(AssetsManager::assetsPath + filename);
	std::string content((std::istreambuf_iterator <char>(inputStream)), (std::istreambuf_iterator <char>()));

	/// String which are used to define XML nodes
	std::string nodeName;
	std::string nodeValue;
	std::string attributeName;
	std::string attributeValue;

	/// Initial values of the XML reader
	State state = Normal;
	XMLNode *node = this;

	for (auto &c : content)
	{
		switch (state)
		{
			case Normal:
				switch (c)
				{
					case '<': state = LowerThan; break;
					case '\n': case ' ': case '\t': break;
					default: // Text node creation
						node = &node->addChild("");
						nodeValue = c;
						state = ReadingTextNode;
				}
			break;

			case LowerThan:
				if (c == '?')
				{
					state = WaitingForGreaterThan;
				}
				else if (isalpha(c))
				{
					nodeName = c;
					state = ReadingNodeName;
					node = &node->addChild("");
				}
				else if (c == '/')
				{
					state = ReadingNodeCloseTag;
				}
			break;

			case ReadingAttributeName:
				switch (c)
				{
					case '=': state = WaitingForQuotationMark; break;
					case ' ': case '\t': case '\n': state = WaitingForEqual; break;
					default: attributeName += c;
				}
			break;

			case ReadingAttributeValue:
				if (c == '"')
				{
					node->setAttribute(attributeName, attributeValue);
					state = WaitingForGreaterThan;
				}
				else
				{
					attributeValue += c;
				}
			break;

			case ReadingNodeName:
				switch (c)
				{
					case '>': node->setName(nodeName); state = Normal; break;
					case ' ': case '\t': case '\n': node->setName(nodeName); state = WaitingForGreaterThan; break;
					default: nodeName += c;
				}
			break;

			case ReadingNodeCloseTag:
				if (c == '>')
				{
					state = Normal;
					node = node->parent;
				}
			break;

			case ReadingTextNode:
				if (c == '<')
				{
					state = Normal;
					node->setValue(nodeValue);
					node = node->parent;
				}
				else
				{
					nodeValue += c;
				}
			break;

			case WaitingForEqual:
				if (c == '=')
					state = WaitingForQuotationMark;
			break;

			case WaitingForGreaterThan:
				if (isalpha(c))
				{
					attributeName = c;
					state = ReadingAttributeName;
					break;
				}

				switch (c)
				{
					case '/': node = node->parent; break;
					case '>': state = Normal;
				}
			break;

			case WaitingForQuotationMark:
				if (c == '"')
				{
					attributeValue = "";
					state = ReadingAttributeValue;
				}
		}
	}
}

///////////////////////////////////
void XMLNode::setName(const std::string &name)
{
	this->name = name;
}

///////////////////////////////////
const std::string & XMLNode::getName() const
{
	return name;
}

///////////////////////////////////
void XMLNode::setValue(const std::string &value)
{
	this->value = value;
}

///////////////////////////////////
const std::string & XMLNode::getValue() const
{
	return value;
}

///////////////////////////////////
void XMLNode::setAttribute(const std::string &attributeName, const std::string &attributeValue)
{
	/// Look for the attribute
	for (auto &attribute : attributes)
	{
		if (attribute.name == attributeName)
		{
			attribute.value = attributeValue;
			return;
		}
	}

	attributes.emplace_back(attributeName, attributeValue);
}

///////////////////////////////////
const std::string & XMLNode::getAttribute(const std::string &attributeName)
{
	for (auto &attribute : attributes)
		if (attribute.name == attributeName)
			return attribute.value;

	return XMLNode::emptyString;
}

///////////////////////////////////
const std::vector <XMLAttribute> & XMLNode::getAttributes() const
{
	return attributes;
}

///////////////////////////////////
XMLNode * XMLNode::getParent()
{
	return parent;
}

///////////////////////////////////
const XMLNode * XMLNode::getParent() const
{
	return parent;
}

///////////////////////////////////
XMLNode & XMLNode::addChild(const std::string &childName)
{
	children.emplace_back(this, childName);
	return children.back();
}

///////////////////////////////////
void XMLNode::addTextChild(const std::string &childValue)
{
	children.emplace_back(this);
	children.back().setValue(childValue);
}

///////////////////////////////////
XMLNode * XMLNode::getChildByName(const std::string &childName)
{
	for (auto &child : children)
		if (child.name == childName)
			return &child;

	return nullptr;
}

///////////////////////////////////
const XMLNode * XMLNode::getChildByName(const std::string &childName) const
{
	return getChildByName(childName);
}

///////////////////////////////////
const std::vector <XMLNode> & XMLNode::getChildren() const
{
	return children;
}

///////////////////////////////////
std::vector <XMLNode *> XMLNode::getChildrenByName(const std::string &childName)
{
	std::vector <XMLNode *> nodes;
	for (auto &child : children)
		if (child.name == childName)
			nodes.emplace_back(&child);

	return nodes;
}

///////////////////////////////////
std::vector <const XMLNode *> XMLNode::getChildrenByName(const std::string &childName) const
{
	return getChildrenByName(childName);
}

///////////////////////////////////
bool XMLNode::write(const std::string &filename) const
{
	std::ofstream xmlFile(filename);

	if (! xmlFile.is_open())
		return false;

	xmlFile << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
	xmlFile << toString();

	xmlFile.close();
	return true;
}

///////////////////////////////////
std::string XMLNode::toString(int tabLevel) const
{
	std::string tabs;
	for (int i = 0; i < tabLevel; i++)
		tabs += "\t";

	if (! name.size())
		return tabs + value;

	std::string output = tabs + "<" + name;

	for (auto &attribute : attributes)
		output += " " + attribute.toString();

	if (! children.size())
	{
		output += " />";
		return output;
	}

	output += ">\n";

	for (auto &child : children)
	{
		output += child.toString(tabLevel + 1);
		output += "\n";
	}

	output += tabs + "</" + name + ">";
	return output;
}

} // namespace lk
