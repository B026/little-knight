/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/Action.hpp>

namespace lk
{
/////////////////////////////////////
int Action::infiniteLoop;

/////////////////////////////////////
Action::Action(float duration, int loopCount, Entity *target) : duration(duration), currentTime(0), loopCount(loopCount), currentLoop(0),
																state(Stopped), target(target)
{
}

/////////////////////////////////////
Action::~Action()
{
}

/////////////////////////////////////
Action * Action::clone() const
{
	return new Action(*this);
}

/////////////////////////////////////
void Action::setTarget(Entity *target)
{
	this->target = target;
}

/////////////////////////////////////
const Entity * Action::getTarget() const
{
	return target;
}

/////////////////////////////////////
Entity * Action::getTarget()
{
	return target;
}

/////////////////////////////////////
void Action::setDuration(float duration)
{
	if (state != Stopped)
		return;

	this->duration = duration;
}

/////////////////////////////////////
float Action::getDuration() const
{
	return duration;
}

/////////////////////////////////////
void Action::setLoopCount(int loopCount)
{
	if (state != Stopped)
		return;

	this->loopCount = loopCount;
}

/////////////////////////////////////
int Action::getLoopCount() const
{
	return loopCount;
}

/////////////////////////////////////
bool Action::isRunning() const
{
	return state == Running;
}

/////////////////////////////////////
bool Action::isPaused() const
{
	return state == Paused;
}

/////////////////////////////////////
bool Action::isStopped() const
{
	return state == Stopped;
}

/////////////////////////////////////
Action::State Action::getState() const
{
	return state;
}

/////////////////////////////////////
void Action::play()
{
	if (! target)
		return;

	state = Running;
}

/////////////////////////////////////
void Action::pause()
{
	if (state == Running)
		state = Paused;
}

/////////////////////////////////////
void Action::stop()
{
	state = Stopped;
	currentTime = 0.f;
	currentLoop = 0;
	onStop.emit();
}

/////////////////////////////////////
void Action::restart()
{
	stop();
	restart();
}

/////////////////////////////////////
void Action::update(float frameTime)
{
	if (! isRunning())
		return;

	if (currentTime + frameTime >= duration)
	{
		onUpdate(duration - currentTime);

		currentTime = 0;
		currentLoop++;

		if (currentLoop >= loopCount && loopCount != infiniteLoop)
			stop();
	}
	else
	{
		currentTime += frameTime;
		onUpdate(frameTime);
	}
}

/////////////////////////////////////
void Action::onUpdate(float frameTime)
{
}

/////////////////////////////////////
void Action::setState(State state)
{
	this->state = state;
}

/////////////////////////////////////
void Action::increaseCurrentLoop()
{
	this->currentLoop++;
}

/////////////////////////////////////
int Action::getCurrentLoop() const
{
	return currentLoop;
}

} // namespace lk
