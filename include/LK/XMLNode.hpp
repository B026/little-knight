/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#ifndef LITTLE_KNIGHT_XML_NODE_HPP
#define LITTLE_KNIGHT_XML_NODE_HPP

#include <LK/XMLAttribute.hpp>
#include <string>
#include <vector>

namespace lk
{

class XMLNode
{
public:
	XMLNode(XMLNode *parent = nullptr, const std::string &name = "");
	XMLNode(const std::string &filename);

	void setName(const std::string &name);
	const std::string & getName() const;

	void setValue(const std::string &value);
	const std::string & getValue() const;

	void setAttribute(const std::string &attributeName, const std::string &attributeValue);
	const std::string & getAttribute(const std::string &attributeName);
	const std::vector <XMLAttribute> & getAttributes() const;

	XMLNode * getParent();
	const XMLNode * getParent() const;

	XMLNode & addChild(const std::string &childName);
	void addTextChild(const std::string &childValue);
	XMLNode * getChildByName(const std::string &childName);
	const XMLNode * getChildByName(const std::string &childName) const;
	const std::vector <XMLNode> & getChildren() const;
	std::vector <XMLNode *> getChildrenByName(const std::string &childName);
	std::vector <const XMLNode *> getChildrenByName(const std::string &childName) const;

	std::string toString(int tabLevel = 0) const;
	bool write(const std::string &filename) const;

private:
	static std::string emptyString;

	std::string name;
	std::string value;
	std::vector <XMLAttribute> attributes;
	XMLNode *parent;
	std::vector <XMLNode> children;
};

} // namespace lk

#endif // LITTLE_KNIGHT_XML_NODE_HPP
