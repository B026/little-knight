/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#ifndef LITTLE_KNIGHT_ABSTRACT_SLIDER_HPP
#define LITTLE_KNIGHT_ABSTRACT_SLIDER_HPP

#include <LK/Sprite.hpp>
#include <LK/Widget.hpp>

namespace lk
{

class AbstractBar : public Widget
{
public:
	/////////////////////////////////////
	/// Defines possible orienations of the
	/// AbstractBar
	/////////////////////////////////////
	enum Orientation
	{
		Horizontal,
		Vertical
	};

	/////////////////////////////////////
	/// Default constructor
	/////////////////////////////////////
	AbstractBar(bool focusable = false);

	/////////////////////////////////////
	/// Mega constructor. Sets
	/// background and filling texutre,
	/// maxValue, value, scene, orientation
	/// and if the widget is focusable or not
	/////////////////////////////////////
	AbstractBar(const std::string &backgroundTexture, const std::string &fillingTexture, int maxValue, int value, Orientation orientation = Horizontal,
				bool focusable = false);

	/////////////////////////////////////
	/// Sets the value of the AbstractBar.
	/// Emits onValueChange
	///
	/// If it's < 0, it's set as 0.
	/// If it's > maxValue, it's set as maxValue
	/////////////////////////////////////
	void setValue(int value);

	/////////////////////////////////////
	///	Gets the value of the AbstractBar
	/////////////////////////////////////
	int getValue() const;

	/////////////////////////////////////
	/// Sets the maxValue of the AbstractBar.
	///
	/// If it's < 0, its absolute value
	/// is used instead
	/////////////////////////////////////
	void setMaxValue(int maxValue);

	/////////////////////////////////////
	/// Gets the maxValue of the AbstractBar
	/////////////////////////////////////
	int getMaxValue() const;

	/////////////////////////////////////
	/// Sets the orientation of the AbstractBar
	/////////////////////////////////////
	void setOrientation(Orientation orientation);

	/////////////////////////////////////
	/// Gets the orientation of the AbstractBar
	/////////////////////////////////////
	Orientation getOrientation() const;

	/////////////////////////////////////
	/// Sets the backgroundTexture
	/////////////////////////////////////
	void setBackgroundTexture(const std::string &backgroundTexture);
	void setBackgroundTexture(sf::Texture *backgroundTexture);

	/////////////////////////////////////
	/// Gets the backgroundTexture
	/////////////////////////////////////
	void setFillingTexture(const std::string &fillingTexture);
	void setFillingTexture(sf::Texture *fillingTexture);

	/// Reimplemented Entity methods

	/////////////////////////////////////
	/// Sets a property using a XMLAttribute
	/////////////////////////////////////
	bool setProperty(const XMLAttribute &property) override;

	/// Signals

	/////////////////////////////////////
	/// Emitted when the value is changed.
	/// Emits the new value
	/////////////////////////////////////
	Signal <int> onValueChange;

protected:
	/////////////////////////////////////
	/// Reposition the components. Only if
	/// componentsNeedUpdate is true
	/////////////////////////////////////
	void onDraw(sf::RenderTarget &target, sf::RenderStates states) const override;

private:
	/////////////////////////////////////
	/// Updates the pixelsPerValue member
	///
	/// This function is called when maxValue
	/// or
	/////////////////////////////////////
	void updatePixelsPerValue();

	/////////////////////////////////////
	/// Current value. It's in the range
	/// [0, maxValue]
	/////////////////////////////////////
	int value;

	/////////////////////////////////////
	/// The maximum value.
	/////////////////////////////////////
	int maxValue;

	/////////////////////////////////////
	/// How many pixels represent value = 1
	/// in the fillingSprite's texture
	/////////////////////////////////////
	float pixelsPerValue;

	/////////////////////////////////////
	/// The orientation of the widget.
	/// Tells how the widget must be drawn
	/////////////////////////////////////
	Orientation orientation;

	/////////////////////////////////////
	/// Tells if the components need to
	/// be updated
	/////////////////////////////////////
	mutable bool componentsNeedUpdate;

	/////////////////////////////////////
	/// Background component. It's name is
	/// "backgroundSprite"
	/////////////////////////////////////
	mutable Sprite *backgroundSprite;

	/////////////////////////////////////
	/// Filling compoent. It's name is
	/// "fillingSprite"
	/////////////////////////////////////
	mutable Sprite *fillingSprite;
};

} // namespace

#endif // LITTLE_KNIGHT_ABSTRACT_SLIDER_HPP
