/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#ifndef LITTLE_KNIGHT_JSON_HPP
#define LITTLE_KNIGHT_JSON_HPP

#include <string>
#include <vector>

namespace lk
{

class JSON
{
public:
	JSON(const std::string &jsonString = "");
	void fromString(const std::string &jsonString);

	void set(const std::string &name, int value);
	void set(const std::string &name, bool value);
	void set(const std::string &name, float value);
	void set(const std::string &name, const std::string &value);

	int getInt(const std::string &name) const;
	bool getBool(const std::string &name) const;
	float getFloat(const std::string &name) const;
	const std::string & getValue(const std::string &name) const;

private:
	struct Value
	{
		std::string name;
		std::string value;

		Value(const std::string &name, const std::string &value);
	};

	static std::string emptyString;
	std::vector <Value> values;
};

} // namespace lk

#endif // LITTLE_KNIGHT_JSON_HPP
