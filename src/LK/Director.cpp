/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/Director.hpp>
#include <LK/Scene.hpp>

namespace lk
{
/////////////////////////////////////
Director & Director::getInstance()
{
	static Director director;
	return director;
}

/////////////////////////////////////
Director::Director()
{
}

/////////////////////////////////////
Director::~Director()
{
	clearScenes();
}

/////////////////////////////////////
int Director::startGame(Scene *scene)
{
	if (! scene)
		return 0;

	scenes.push(scene);
	scene->onCreate();

	while (! scenes.empty())
	{
		auto *scene = scenes.top();
		auto &directorAction = scene->run();

		switch (directorAction.type)
		{
			case DirectorAction::PushScene:
				scenes.push(directorAction.scene);
				directorAction.scene->onCreate();
			break;

			case DirectorAction::PopScene:
				delete scenes.top();
				scenes.pop();

				if (scenes.size())
					scenes.top()->onReopen();
			break;

			case DirectorAction::PopPushScene:
				delete scenes.top();
				scenes.pop();
				scenes.push(directorAction.scene);
				directorAction.scene->onCreate();
			break;

			case DirectorAction::ClearPushScene:
				clearScenes();
				scenes.push(directorAction.scene);
				directorAction.scene->onCreate();
			break;

			case DirectorAction::CloseGame:
				clearScenes();
		}
	}

	return 0;
}

/////////////////////////////////////
Scene * Director::getScene()
{
	auto &director = Director::getInstance();
	return (! director.scenes.size()) ? nullptr : director.scenes.top();
}


/////////////////////////////////////
void Director::clearScenes()
{
	while (! scenes.empty())
	{
		delete scenes.top();
		scenes.pop();
	}
}

} // namespace lk
