/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/Shape.hpp>
#include <LK/Entity.hpp>
#include <SFML/Graphics/VertexArray.hpp>

namespace lk
{
/////////////////////////////////////
Shape::Shape() : entity(nullptr)
{
}

/////////////////////////////////////
void Shape::setEntity(Entity *entity)
{
	this->entity = entity;
}

/////////////////////////////////////
void Shape::pushVertex(float vertexX, float vertexY)
{
	vertices.emplace_back(vertexX, vertexY);
}

/////////////////////////////////////
void Shape::pushVertex(const Vector2D &vertex)
{
	vertices.push_back(vertex);
}

/////////////////////////////////////
std::vector <Vector2D> Shape::getTransformedVertices() const
{
	std::vector <Vector2D> transformedVertices;
	const auto &entityTransform = entity->getFullTransform();

	for (auto &vertex : vertices)
		transformedVertices.emplace_back(entityTransform.transformPoint(vertex));

	return transformedVertices;
}

/////////////////////////////////////
int Shape::getVertexCount() const
{
	return vertices.size();
}

/////////////////////////////////////
std::vector <Vector2D> Shape::getAxes() const
{
	std::vector <Vector2D> axes;
	getAxes(axes);

	return axes;
}

/////////////////////////////////////
void Shape::getAxes(std::vector <Vector2D> &axes) const
{
	if (! vertices.size())
		return;

	const auto &transformedVertices = getTransformedVertices();

	for (int i = 0; i < transformedVertices.size() - 1; i++)
	{
		const auto &edge = transformedVertices [i] - transformedVertices [i + 1];
		axes.emplace_back(Vector2D(- edge.y, edge.x).getUnit());
	}

	const auto &edge = transformedVertices [transformedVertices.size() - 1] - transformedVertices [0];
	axes.emplace_back(Vector2D(- edge.y, edge.x).getUnit());
}

/////////////////////////////////////
Projection Shape::getProjection(const Vector2D &axis) const
{
	if (! vertices.size())
		return Projection();

	const auto &transformedVertices = getTransformedVertices();
	float dot = transformedVertices [0].dot(axis);
	Projection projection(dot, dot);

	for (auto it = transformedVertices.begin() + 1; it != transformedVertices.end(); ++it)
	{
		dot = it->dot(axis);
		Projection newProjection(dot, dot);

		projection.min = (newProjection.min < projection.min) ? newProjection.min : projection.min;
		projection.max = (newProjection.max > projection.max) ? newProjection.max : projection.max;
	}

	return projection;
}

void Shape::draw(sf::RenderTarget &target)
{
	sf::VertexArray drawable;
	drawable.setPrimitiveType(sf::LineStrip);

	const auto &transformedVertices = getTransformedVertices();

	for (auto &vertex : transformedVertices)
		drawable.append(sf::Vertex(vertex, sf::Color::Green));

	target.draw(drawable);
}

/////////////////////////////////////
Shape Shape::boxShape(const Vector2D &size)
{
	Shape box;
	box.pushVertex(0, 0);
	box.pushVertex(size.x, 0);
	box.pushVertex(size.x, size.y);
	box.pushVertex(0, size.y);

	return box;
}

} // namespace lk
