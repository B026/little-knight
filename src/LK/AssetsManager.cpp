/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/AssetsManager.hpp>

namespace lk
{
/////////////////////////////////////////
std::string AssetsManager::assetsPath;

/////////////////////////////////////
sf::Texture * AssetsManager::getTexture(const std::string &texturePath)
{
	auto &textures = AssetsManager::getInstance().textures;
	auto fullPath = AssetsManager::assetsPath + texturePath;

	/// Check if it's already loaded
	for (auto *texture : textures)
		if (texture->path == fullPath)
			return &texture->asset;

	/// Not loaded yet, load it
	auto *texture = new Asset <sf::Texture>(fullPath);
	if (! texture->asset.loadFromFile(fullPath))
	{
		delete texture;
		return nullptr;
	}

	textures.push_back(texture);
	return &texture->asset;
}

/////////////////////////////////////
void AssetsManager::deleteTexture(const std::string &texturePath)
{
	auto &textures = AssetsManager::getInstance().textures;
	auto fullPath = AssetsManager::assetsPath + texturePath;

	for (auto it = textures.begin(); it != textures.end(); ++it)
	{
		if ((*it)->path == fullPath)
		{
			delete *it;
			textures.erase(it);
			return;
		}
	}
}

/////////////////////////////////////
void AssetsManager::deleteTextures()
{
	auto &textures = AssetsManager::getInstance().textures;

	for (auto *texture : textures)
		delete texture;

	textures.clear();
}

/////////////////////////////////////
sf::Font * AssetsManager::getFont(const std::string &fontPath)
{
	auto &fonts = AssetsManager::getInstance().fonts;
	auto fullPath = AssetsManager::assetsPath + fontPath;

	/// Check if it's already loaded
	for (auto *font : fonts)
		if (font->path == fullPath)
			return &font->asset;

	/// Not loaded yet, load it
	auto *font = new Asset <sf::Font>(fullPath);
	if (! font->asset.loadFromFile(fullPath))
	{
		delete font;
		return nullptr;
	}

	fonts.push_back(font);
	return &font->asset;
}

/////////////////////////////////////
void AssetsManager::deleteFont(const std::string &fontPath)
{
	auto &fonts = AssetsManager::getInstance().fonts;
	auto fullPath = AssetsManager::assetsPath + fontPath;

	for (auto it = fonts.begin(); it != fonts.end(); ++it)
	{
		if ((*it)->path == fullPath)
		{
			delete *it;
			fonts.erase(it);
			return;
		}
	}
}

/////////////////////////////////////
void AssetsManager::deleteFonts()
{
	auto &fonts = AssetsManager::getInstance().fonts;

	for (auto *font : fonts)
		delete font;

	fonts.clear();
}

/////////////////////////////////////
sf::SoundBuffer * AssetsManager::getSound(const std::string &filename)
{
	auto &sounds = AssetsManager::getInstance().sounds;
	std::string fullPath(AssetsManager::assetsPath + filename);

	/// Check if it has been loaded
	for (auto *sound : sounds)
		if (sound->path == fullPath)
			return &sound->asset;

	/// Try to load it
	auto *sound = new Asset <sf::SoundBuffer>(fullPath);
	if (! sound->asset.loadFromFile(fullPath))
	{
		delete sound;
		return nullptr;
	}

	sounds.push_back(sound);
	return &sound->asset;
}

/////////////////////////////////////
void AssetsManager::deleteSound(const std::string &filename)
{
	auto &sounds = AssetsManager::getInstance().sounds;

	for (auto it = sounds.begin(); it != sounds.end(); ++it)
	{
		if ((*it)->path == filename)
		{
			delete *it;
			sounds.erase(it);
			return;
		}
	}
}

/////////////////////////////////////
void AssetsManager::deleteSounds()
{
	auto &sounds = AssetsManager::getInstance().sounds;

	for (auto *sound : sounds)
		delete sound;

	sounds.clear();
}

/////////////////////////////////////
SpriteAnimation * AssetsManager::loadSpriteAnimation(const std::string &spriteAnimationName, const std::string &texturePath,
													 const Vector2D &frameSize, int numberOfFrames, int row, int column)
{
	return AssetsManager::loadSpriteAnimation(spriteAnimationName, AssetsManager::getTexture(texturePath), frameSize, numberOfFrames,
											  row, column);
}

/////////////////////////////////////
SpriteAnimation * AssetsManager::loadSpriteAnimation(const std::string &spriteAnimationName, sf::Texture *texture,
													 const Vector2D &frameSize, int numberOfFrames, int row, int column)
{
	if (! texture)
		return nullptr;

	auto *spriteAnimation = new SpriteAnimation(spriteAnimationName, texture, frameSize, numberOfFrames, row, column);
	AssetsManager::getInstance().spriteAnimations.emplace_back(spriteAnimation);
	return spriteAnimation;
}

/////////////////////////////////////
SpriteAnimation * AssetsManager::getSpriteAnimation(const std::string &spriteAnimationName)
{
	auto &spriteAnimations = AssetsManager::getInstance().spriteAnimations;

	for (auto *spriteAnimation : spriteAnimations)
		if (spriteAnimation->getName() == spriteAnimationName)
			return spriteAnimation;

	return nullptr;
}

/////////////////////////////////////
void AssetsManager::deleteSpriteAnimation(const std::string &spriteAnimationName)
{
	auto &spriteAnimations = AssetsManager::getInstance().spriteAnimations;

	for (auto it = spriteAnimations.begin(); it != spriteAnimations.end(); ++it)
	{
		if ((*it)->getName() == spriteAnimationName)
		{
			delete *it;
			spriteAnimations.erase(it);
			return;
		}
	}
}

/////////////////////////////////////
void AssetsManager::deleteSpriteAnimations()
{
	auto &spriteAnimations = AssetsManager::getInstance().spriteAnimations;

	for (auto *spriteAnimation : spriteAnimations)
		delete spriteAnimation;

	spriteAnimations.clear();
}

/////////////////////////////////////
AssetsManager::AssetsManager()
{
}

/////////////////////////////////////
AssetsManager::~AssetsManager()
{
	deleteTextures();
	deleteFonts();
	deleteSounds();
	deleteSpriteAnimations();
}

/////////////////////////////////////
AssetsManager & AssetsManager::getInstance()
{
	static AssetsManager assetsManager;
	return assetsManager;
}

} // namespace lk
