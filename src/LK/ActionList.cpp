/////////////////////////////////////////////////////////////////////////////////
// Little Knight - Copyright (c) 2018
// Alberto Castro Estrada            (albertoeisc@gmail.com)
//
// This software is provided 'as-is', without any express or implied warranty.
// In no event will the authors be held liable for any damages arising from
// the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not claim
//    that you wrote the original software. If you use this software in a product,
//    an acknowledgment in the product documentation would be appreciated but is
//    not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
//    misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
/////////////////////////////////////////////////////////////////////////////////

#include <LK/ActionList.hpp>

namespace lk
{
/////////////////////////////////////
ActionList::ActionList() : currentAction(nullptr), currentActionIndex(-1)
{
}

/////////////////////////////////////
ActionList::~ActionList()
{
	clear();
}

/////////////////////////////////////
void ActionList::pushAction(const Action &action)
{
	auto newAction = action.clone();

	if (! newAction->getTarget())
		newAction->setTarget(target);

	actions.push_back(newAction);
}

/////////////////////////////////////
void ActionList::clear()
{
	stop();

	for (auto *action : actions)
		delete action;

	actions.clear();
}

/////////////////////////////////////
void ActionList::play()
{
	if (! actions.size())
		return;

	setState(Running);
	currentActionIndex = 0;
	currentAction = actions [currentActionIndex];
	currentAction->play();
}

/////////////////////////////////////
void ActionList::pause()
{
	if (isPaused())
		return;

	setState(Paused);
	currentAction->pause();
}

/////////////////////////////////////
void ActionList::stop()
{
	setState(Stopped);

	if (currentAction)
		currentAction->stop();

	currentAction = nullptr;
	currentActionIndex = -1;

	Action::stop();
}

/////////////////////////////////////
void ActionList::update(float deltaTime)
{
	if (! isRunning())
		return;

	currentAction->update(deltaTime);

	if (currentAction->isStopped())
	{
		currentActionIndex++;

		if (currentActionIndex < actions.size())
		{
			currentAction = actions [currentActionIndex];
			currentAction->play();
		}
		else
		{
			increaseCurrentLoop();
			int loopCount = getLoopCount();

			if (getCurrentLoop() >= loopCount && loopCount != Action::infiniteLoop)
				stop();
			else
			{
				currentActionIndex = 0;
				currentAction = actions [currentActionIndex];
				currentAction->play();
			}
		}
	}
}

} // namespace lk
